import sbtassembly.Plugin._
import AssemblyKeys._
import sbt._
import sbt.Keys._

object ObdscanScalaBuild extends Build {
  val scalaVer = "2.9.2"

  lazy val obdscanScala = Project(
    id = "obdscan-scala",
    base = file("."),
    settings = Project.defaultSettings ++ assemblySettings ++ Seq(
      name := "OBDScan Scala",
      organization := "thesis.bert",
      version := "0.1-SNAPSHOT",
      scalaVersion := scalaVer,

      // add other settings here
      mainClass in assembly := Some("thesis.bert.ObdscanScala"),

      // resolvers
      resolvers ++= Seq (
          "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
          "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
      ),

      // dependencies
      libraryDependencies ++= Seq (
        "org.scala-lang" % "scala-swing" % scalaVer,
        "org.rxtx" % "rxtx" % "2.1.7",
        "com.h2database" % "h2" % "1.3.168",
        "org.scalaquery" % "scalaquery_2.9.1-1" % "0.10.0-M1",
        "org.swinglabs" % "swingx" % "1.6.1",
        "org.scalanlp" %% "breeze-viz" % "0.1",
        "org.scalatest" %% "scalatest" % "1.8" % "test"
      )
    )
  )
}

package thesis.bert.gui

import gnu.io._
import java.awt.Font
import scala.swing._
import scala.swing.event._
import thesis.bert.gui.components.SerialTerminal
import thesis.bert.lib.helper._
import thesis.bert.lib.serial._
import thesis.bert.model._
import scala.swing.event.KeyReleased
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator
import thesis.bert.gui.components.AutoCompleteComboBox

class PowerMode()(implicit serialPort: Serial) extends SplitPane {
  orientation = Orientation.Horizontal
  dividerSize = 0

  val field = new AutoCompleteComboBox(Array(""));

  val terminal = new SerialTerminal

  def activate = {
    serialPort.addListener(terminal)
  }

  def deactivate = {
    serialPort.removeListener
  }

  def addItem(item: String) = {
    field.peer.addItem(item)
  }

  val userInput = new FlowPanel {
    val label = new Label("Input command:")

    contents += label
    contents += field

    listenTo(field.selection)
    reactions += {
      case SelectionChanged(`field`) => {
        val command = field.selection.item.substring(0, 5)
        serialPort.messenger ! command
      }
    }
  }

  // remember, we want the terminal to be scrollable
  // because user is expected to use this for more than
  // one command.
  val terminalContainer = new ScrollPane(terminal)

  leftComponent = userInput
  rightComponent = terminalContainer
}
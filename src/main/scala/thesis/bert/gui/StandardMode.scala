package thesis.bert.gui

import scala.swing.Publisher
import scala.swing.TabbedPane
import scala.swing.Button
import thesis.bert.lib.serial.Serial
import scala.swing.event.SelectionChanged
import scala.swing.FlowPanel
import scala.swing.event.FontChanged
import scala.swing.event.SelectionChanged
import scala.swing.event.ListSelectionChanged

class StandardMode(implicit serialPort: Serial) extends TabbedPane {
  import TabbedPane._
  import thesis.bert.gui.controls._

  var previousPageIndex = 0
  var previousPageListIndex = 0
  var currentPageIndex = 0
  var currentPageListIndex = 0

  this.peer.addChangeListener(new javax.swing.event.ChangeListener {
    def stateChanged(e: javax.swing.event.ChangeEvent) {
      previousPageIndex = currentPageIndex
      previousPageListIndex = currentPageListIndex

      currentPageIndex = selection.page.index
      currentPageListIndex = selection.page.content.asInstanceOf[ControlPanel].list.currentItemIndex
    }
  })

  val DTC = new DTC
  val Index = new Index
  val Chart = new Chart
  val Diagnostic = new ControlPanel("Diagnostics", Seq(Index, DTC, Chart))

  val EngineInfo = new EngineInfo
  val Engine = new ControlPanel("Engine", Seq(EngineInfo)) {
    // we want an empty left side since there's only one menu here
    leftComponent = new FlowPanel
  }

  val ExhaustInfo = new ExhaustInfo
  val Exhaust = new ControlPanel("Exhaust", Seq(ExhaustInfo)) {
    leftComponent = new FlowPanel
  }

  val FuelInfo = new FuelInfo
  val FuelPressure = new FuelPressure
  val Fuel = new ControlPanel("Fuel", Seq(FuelInfo, FuelPressure))

  val IntakeManifoldInfo = new IntakeManifoldInfo
  val IntakeManifold = new ControlPanel("Intake Manifold", Seq(IntakeManifoldInfo)) {
    leftComponent = new FlowPanel
  }

  val O2SensorExistence = new O2SensorExistence
  val O2SensorVoltage = new O2SensorVoltage
  val O2Sensor = new ControlPanel("O2 Sensor", Seq(O2SensorExistence, O2SensorVoltage))

  val SupportInfo = new Support
  val Support = new ControlPanel("Support", Seq(SupportInfo)) {
    leftComponent = new FlowPanel
  }

  pages += new Page(Diagnostic.title, Diagnostic)
  pages += new Page(Engine.title, Engine)
  pages += new Page(Fuel.title, Fuel)
  pages += new Page(IntakeManifold.title, IntakeManifold)
  pages += new Page(Exhaust.title, Exhaust)
  pages += new Page(O2Sensor.title, O2Sensor)
  //  pages += new Page(Support.title, Support)

  def activate = {
    serialPort.addListener(selection.page.content.asInstanceOf[ControlPanel].list.selection.items(0))
    selection.page.content.asInstanceOf[ControlPanel].list.selection.items(0).startOperation
  }

  def deactivate = {
    serialPort.removeListener
    selection.page.content.asInstanceOf[ControlPanel].list.selection.items(0).stopOperation
  }

  def deactivateAll = {
    serialPort.removeListener
    pages(currentPageIndex).content.asInstanceOf[ControlPanel].list.listData(currentPageListIndex).stopOperation
  }

  listenTo(Diagnostic.list.selection, Engine.list.selection, Fuel.list.selection, IntakeManifold.list.selection, Exhaust.list.selection, O2Sensor.list.selection)
  reactions += {
    case ListSelectionChanged(_, _, _) => {

      previousPageListIndex = currentPageListIndex
      currentPageListIndex = selection.page.content.asInstanceOf[ControlPanel].list.currentItemIndex
    }
  }
}

package thesis.bert.gui

import java.awt.Image
import java.net.URL
import javax.swing.ImageIcon
import scala.actors.Actor
import scala.actors.Actor._
import scala.actors.TIMEOUT
import scala.io.Source
import scala.swing._
import scala.swing.event.SelectionChanged
import thesis.bert.lib.serial.Serial
import thesis.bert.lib.serial.SerialListener
import javax.swing.event.ListSelectionListener

case class ControlPanel(title: String, elems: Seq[ControlPanelContent])(implicit serialPort: Serial) extends SplitPane {
  orientation = Orientation.Vertical
  dividerSize = 0

  val list = new ListView(elems) {
    selectIndices(0)
    selection.intervalMode = ListView.IntervalMode.Single
    renderer = ListView.Renderer(_.icon)

    var previousItemIndex = 0
    var currentItemIndex = 0

    peer.getSelectionModel.addListSelectionListener(new ListSelectionListener {
      def valueChanged(e: javax.swing.event.ListSelectionEvent) {
        if (!e.getValueIsAdjusting) {
          previousItemIndex = currentItemIndex
          currentItemIndex = peer.getSelectedIndex

          listData(previousItemIndex).stopOperation
          listData(currentItemIndex).startOperation
        }
      }
    })
  }

  leftComponent = list
  rightComponent = list.selection.items(0)

  listenTo(list.selection)
  reactions += {
    case SelectionChanged(`list`) => {
      if (list.selection.items.length == 1) {
        serialPort.removeListener
        serialPort.addListener(list.selection.items(0))
        rightComponent = list.selection.items(0)
      }
    }
  }
}

abstract case class ControlPanelContent(title: String, iconName: String)(implicit serialPort: Serial) extends FlowPanel with SerialListener {
  name = title
  val icon: ImageIcon = createIcon(iconName, ControlPanelContent.DefaultIconSize)

  val pageTitle = new Label(title)
  pageTitle.font = ControlPanelContent.TitleFont

  val commands: Seq[String]

  val communicator = actor {
    var currentCommand = 0

    loop {
      react {
        case 'start => {
          val commandLength = commands.length - 1
          currentCommand = if (currentCommand == commandLength) 0 else currentCommand + 1
          serialPort.messenger ! commands(currentCommand)
        }
        case 'stop => exit
      }
    }
  }

  protected def scheduler(time: Long)(f: => Unit) = {
    def fixedRateLoop {
      Actor.reactWithin(time) {
        case TIMEOUT => f; fixedRateLoop
        case 'stop   => exit
      }
    }
    Actor.actor(fixedRateLoop)
  }

  protected var sched: Actor = _
  def startOperation = {
    if (sched != null) {
      sched.restart
      communicator.restart
    } else {
      sched = scheduler(Serial.SerialPortDelay) {
        communicator ! 'start
      }
    }
  }

  def stopOperation = {
    if (sched != null) {
      sched ! 'stop
      communicator ! 'stop
    }
  }

  protected def createImage(name: String, size: Int) = {
    val path = Option(ControlPanelContent.getImage(name))

    val img = path match {
      case Some(exists) => new ImageIcon(exists).getImage
      case _            => new ImageIcon(ControlPanelContent.DefaultIcon).getImage
    }

    img.getScaledInstance(size, size, Image.SCALE_SMOOTH)
  }

  protected def createIcon(name: String, size: Int): ImageIcon = {
    val path: Option[URL] = Option(ControlPanelContent.getImage(name))

    val img: java.awt.Image = path match {
      case Some(exists) => new ImageIcon(exists).getImage
      case _            => new ImageIcon(ControlPanelContent.DefaultIcon).getImage
    }
    val resizedImg = img.getScaledInstance(size, size, Image.SCALE_SMOOTH)
    new ImageIcon(resizedImg)
  }

  override def readSerial: String = {
    serialPort.queryResult
  }

  override def responseAction(txt: String) = {
    println(txt)
    println
  }
}

object ControlPanelContent {
  val IconPath = "/icons/"
  val DefaultIcon = getClass.getResource(getIconPath("icon"))
  val DefaultTitle = "Panel"
  val DefaultIconSize = 64

  val TitleFont = new Font("Arial", 0, 36)

  def getImage(name: String) = {
    getClass.getResource(getIconPath(name))
  }

  def getIconPath(name: String) = {
    IconPath + name + ".png"
  }
}
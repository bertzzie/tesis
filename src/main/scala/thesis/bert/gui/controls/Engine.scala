package thesis.bert.gui.controls

import java.text.DecimalFormat
import scala.swing._
import Swing._
import thesis.bert.gui.ControlPanel
import thesis.bert.gui.ControlPanelContent
import thesis.bert.lib.serial.Serial
import thesis.bert.model.OBDModel

class EngineInfo(implicit serialPort: Serial) extends ControlPanelContent("Vehicle Engine Information", "icon-engine") {
  val commands: Seq[String] = Seq(
    "01 04",
    "01 05",
    "01 0C",
    "01 1F",
    "01 5C",
    "01 5E",
    "01 61",
    "01 62",
    "01 63",
    "01 64"
  // "09 01",
  // "09 02"
  )

  //region VIN
  val vinLabel = new Label("VIN")
  val vinValue = new Label("00000000")
  val vinInfo = new FlowPanel {
    contents += vinLabel
    contents += vinValue
  }
  //endRegion VIN

  //region Torque
  val torqueLabel = new Label("Ref. Torque")
  val torqueValue = new Label("000")
  val torqueUnit = new Label("Nm")
  val torqueInfo = new FlowPanel {
    contents += torqueLabel
    contents += torqueValue
    contents += torqueUnit
  }

  val torqueDemandLabel = new Label("Demand")
  val torqueDemandValue = new Label("00")
  val torqueDemandUnit = new Label("%")
  val torqueDemandInfo = new FlowPanel {
    contents += torqueDemandLabel
    contents += torqueDemandValue
    contents += torqueDemandUnit
  }

  val torqueActualLabel = new Label("Actual")
  val torqueActualValue = new Label("00")
  val torqueActualUnit = new Label("%")
  val torqueActualInfo = new FlowPanel {
    contents += torqueActualLabel
    contents += torqueActualValue
    contents += torqueActualUnit
  }

  val allTorqueInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(torqueInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(torqueDemandInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(torqueActualInfo, BorderPanel.Position.East)
    }
  }
  //endRegion Torque

  //region RunTime
  val runtimeLabel = new Label("Engine Runtime")
  val runtimeValue = new Label("00:00:00")
  runtimeLabel.font = new Font("Arial", 0, 32)
  runtimeValue.font = new Font("Arial", 0, 32)

  val runtimeInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(runtimeLabel, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(runtimeValue, BorderPanel.Position.Center)
    }
  }
  //endRegion RunTime

  //region TempInfo
  val coolantTempLabel = new Label("Coolant Temp")
  val coolantTempValue = new Label("00")
  val coolantTempUnit = new Label("c")
  val coolantTempInfo = new FlowPanel {
    contents += coolantTempLabel
    contents += coolantTempValue
    contents += coolantTempUnit
  }

  val oilTempLabel = new Label("Oil Temp")
  val oilTempValue = new Label("00")
  val oilTempUnit = new Label("c")
  val oilTempInfo = new FlowPanel {
    contents += oilTempLabel
    contents += oilTempValue
    contents += oilTempUnit
  }

  val tempInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(coolantTempInfo, BorderPanel.Position.West)
    }
    contents += new BorderPanel {
      add(oilTempInfo, BorderPanel.Position.West)
    }
  }
  //endRegion TempInfo

  //region EtcInfo
  val rotationLabel = new Label("Rotation")
  val rotationValue = new Label("Value")
  val rotationUnit = new Label("RPM")
  val rotationInfo = new FlowPanel {
    contents += rotationLabel
    contents += rotationValue
    contents += rotationUnit
  }

  val loadLabel = new Label("Load")
  val loadValue = new Label("00")
  val loadUnit = new Label("%")
  val loadInfo = new FlowPanel {
    contents += loadLabel
    contents += loadValue
    contents += loadUnit
  }

  val fuelRateLabel = new Label("Fuel Rate")
  val fuelRateValue = new Label("000")
  val fuelRateUnit = new Label("L/h")
  val fuelRateInfo = new FlowPanel {
    contents += fuelRateLabel
    contents += fuelRateValue
    contents += fuelRateUnit
  }

  val etcInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(rotationInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(loadInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(fuelRateInfo, BorderPanel.Position.East)
    }
  }
  //endRegion EtcInfo

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new GridPanel(1, 2) {
      contents += new BorderPanel {
        add(vinInfo, BorderPanel.Position.West)
      }
      contents += new BorderPanel {
        add(allTorqueInfo, BorderPanel.Position.East)
      }
    }

    contents += new BorderPanel {
      border = EmptyBorder(15)
      add(runtimeInfo, BorderPanel.Position.Center)
    }

    contents += new GridPanel(1, 2) {
      contents += new BorderPanel {
        add(tempInfo, BorderPanel.Position.West)
      }
      contents += new BorderPanel {
        add(etcInfo, BorderPanel.Position.East)
      }
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "0104" => loadValue.text = formattedResult
        case "0105" => coolantTempValue.text = formattedResult
        case "010C" => rotationValue.text = formattedResult
        case "011F" => {
          val total = calculatedResult._1
          val hour = scala.math.floor(total / 3600.0)
          val minutes = scala.math.floor((total - (hour * 3600.0)) / 60.0)
          val seconds = total - (minutes * 60) - (hour * 3600.0)

          val runtimeFormatter = new DecimalFormat("##")

          runtimeValue.text = runtimeFormatter.format(hour) + ":" + runtimeFormatter.format(minutes) + ":" + runtimeFormatter.format(seconds)
        }
        case "015C" => oilTempValue.text = formattedResult
        case "015E" => fuelRateValue.text = formattedResult
        case "0161" => torqueDemandValue.text = formattedResult
        case "0162" => torqueActualValue.text = formattedResult
        case "0163" => torqueValue.text = formattedResult
        case _      =>
      }
    }
  }
}

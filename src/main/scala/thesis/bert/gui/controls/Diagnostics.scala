package thesis.bert.gui.controls

import java.text.DecimalFormat
import scala.actors._
import scala.actors.Actor._
import scala.swing.Alignment
import scala.swing.BorderPanel
import scala.swing.BoxPanel
import scala.swing.Button
import scala.swing.ComboBox
import scala.swing.FlowPanel
import scala.swing.Font
import scala.swing.GridPanel
import scala.swing.Label
import scala.swing.Orientation
import scala.swing.Swing.EmptyBorder
import scala.swing.Swing.VStrut
import scala.swing.Table
import scala.swing.ToggleButton
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection
import thesis.bert.gui.ControlPanelContent
import thesis.bert.gui.components.AutoCompleteComboBox
import thesis.bert.gui.components.LinearChart
import thesis.bert.lib.serial.Serial
import thesis.bert.model._
import org.scalaquery.session._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql._
import org.scalaquery.ql.extended.H2Driver.Implicit._
import org.scalaquery.ql.TypeMapper._
import thesis.bert.ObdscanScala
import scala.swing.event.SelectionChanged
import scala.swing.event.ButtonClicked
import scala.swing.Slider
import scala.swing.event.ValueChanged
import scala.swing.ScrollPane

class Index(implicit serialPort: Serial) extends ControlPanelContent("Diagnostic Page", "icon-info") {
  val commands: Seq[String] = Seq(
    "01 01",
    "01 05",
    "01 0D",
    "01 1F",
    "01 21",
    "01 41",
    "01 42",
    "01 43",
    "01 46",
    "01 49",
    "01 4A",
    "01 4B",
    "01 4D",
    "01 4D",
    "01 5A",
    "01 5B")

  private val resourcePath = "/resources/icons/"
  private val iconSize = 32

  //region AllComponentsCreation
  //region BatteryInfo
  val batteryTruckIcon = createIcon("icon-truck", iconSize)
  val batteryBattIcon = createIcon("icon-battery", iconSize)
  val batteryValue = new Label("0")
  val batteryUnit = new Label("%")
  val batteryInfo = new FlowPanel {
    contents += new Label("", batteryTruckIcon, Alignment.Left)
    contents += new Label("", batteryBattIcon, Alignment.Left)
    contents += batteryValue
    contents += batteryUnit
  }

  val emptyIcon = createIcon("icon-empty", iconSize)
  val loadIcon = createIcon("icon-workload", iconSize)
  val loadValue = new Label("0")
  val loadUnit = new Label("%")
  val loadInfo = new FlowPanel {
    contents += new Label("", emptyIcon, Alignment.Left)
    contents += new Label("", loadIcon, Alignment.Left)
    contents += loadValue
    contents += loadUnit
  }

  val batteryVoltIcon = createIcon("icon-voltage", iconSize)
  val batteryVoltBatIcon = createIcon("icon-battery", iconSize)
  val batteryVoltValue = new Label("0")
  val batteryVoltUnit = new Label("v")
  val batteryVoltInfo = new FlowPanel {
    contents += new Label("", batteryVoltIcon, Alignment.Left)
    contents += new Label("", batteryVoltBatIcon, Alignment.Left)
    contents += batteryVoltValue
    contents += batteryVoltUnit
  }

  val batteryAndLoadInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(batteryInfo, BorderPanel.Position.West)
    }

    contents += new BorderPanel {
      add(loadInfo, BorderPanel.Position.West)
    }

    contents += new BorderPanel {
      add(batteryVoltInfo, BorderPanel.Position.West)
    }
  }
  //endRegion BatteryInfo

  //region TempInfo
  val ambientTempCloudIcon = createIcon("icon-cloud", iconSize)
  val ambientTempTermoIcon = createIcon("icon-temperature", iconSize)
  val ambientTempValue = new Label("0")
  val ambientTempUnit = new Label("c")
  val ambientTempInfo = new FlowPanel {
    contents += new Label("", ambientTempCloudIcon, Alignment.Left)
    contents += new Label("", ambientTempTermoIcon, Alignment.Left)
    contents += ambientTempValue
    contents += ambientTempUnit
  }

  val engineTempEngineIcon = createIcon("icon-engine", iconSize)
  val engineTempTermoIcon = createIcon("icon-temperature", iconSize)
  val engineTempValue = new Label("0")
  val engineTempUnit = new Label("c")
  val engineTempInfo = new FlowPanel {
    contents += new Label("", engineTempEngineIcon, Alignment.Left)
    contents += new Label("", engineTempTermoIcon, Alignment.Left)
    contents += engineTempValue
    contents += engineTempUnit
  }

  val tempInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(ambientTempInfo, BorderPanel.Position.West)
    }
    contents += new BorderPanel {
      add(engineTempInfo, BorderPanel.Position.West)
    }
  }
  //endRegion TempInfo

  //region MILInfo
  val milLabel = new Label("Malfunction Indicator Lamp (MIL)")
  val milValue = new ToggleButton {
    text = "Off"
    selected = false
  }
  val milInfo = new FlowPanel {
    contents += milLabel
    contents += milValue
  }

  val milDistanceLabel = new Label("Distance since MIL on")
  val milDistanceValue = new Label("0")
  val milDistanceUnit = new Label("km")
  val milDistanceInfo = new FlowPanel {
    contents += milDistanceLabel
    contents += milDistanceValue
    contents += milDistanceUnit
  }

  val milTimeLabel = new Label("Time since MIL on")
  val milTimeValue = new Label("0")
  val milTimeUnit = new Label("min")
  val milTimeInfo = new FlowPanel {
    contents += milTimeLabel
    contents += milTimeValue
    contents += milTimeUnit
  }

  val allMilInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(milInfo, BorderPanel.Position.East)
    }

    contents += new BorderPanel {
      add(milDistanceInfo, BorderPanel.Position.East)
    }

    contents += new BorderPanel {
      add(milTimeInfo, BorderPanel.Position.East)
    }
  }
  //endRegion MILInfo

  //region SpeedInfo
  val speedLabel = new Label("Speed:")
  val speedValue = new Label("000")
  speedValue.font = new Font("Arial", 0, 64)
  val engineRuntimeValue = new Label("0m 0s")
  val runtimeInfo = new FlowPanel {
    contents += new Label("Engine runtime: ")
    contents += engineRuntimeValue
  }

  val speedAndRuntimeInfo = new BoxPanel(Orientation.Vertical) {
    border = EmptyBorder(10)

    contents += new BorderPanel {
      add(speedLabel, BorderPanel.Position.Center)
    }
    contents += new FlowPanel {
      contents += speedValue
      contents += new Label("km/h")
    }
    contents += new BorderPanel {
      add(runtimeInfo, BorderPanel.Position.Center)
    }
  }
  //endRegion SpeedInfo

  //region Actions
  val monitorStatus = new Button("Monitor Status")
  val freezeDTC = new Button("Freeze DTC")
  val actionButtons = new FlowPanel {
    contents += monitorStatus
    contents += freezeDTC
  }
  //endRegion Actions

  //region PedalsInfo
  val relPedalIcon = createIcon("icon-gaspedal", iconSize)
  val relPedalLabel = new Label("rel")
  val relPedalValue = new Label("0")
  val relPedalUnit = new Label("%")
  val relPedal = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(relPedalLabel, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(new Label("", relPedalIcon, Alignment.Center), BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(relPedalValue, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(relPedalUnit, BorderPanel.Position.Center)
    }
  }

  val firstPedalIcon = createIcon("icon-gaspedal", iconSize)
  val firstPedalLabel = new Label("1")
  val firstPedalValue = new Label("0")
  val firstPedalUnit = new Label("%")
  val firstPedal = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(firstPedalLabel, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(new Label("", firstPedalIcon, Alignment.Center), BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(firstPedalValue, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(firstPedalUnit, BorderPanel.Position.Center)
    }
  }

  val secondPedalIcon = createIcon("icon-gaspedal", iconSize)
  val secondPedalLabel = new Label("2")
  val secondPedalValue = new Label("0")
  val secondPedalUnit = new Label("%")
  val secondPedal = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(secondPedalLabel, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(new Label("", secondPedalIcon, Alignment.Center), BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(secondPedalValue, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(secondPedalUnit, BorderPanel.Position.Center)
    }
  }

  val thirdPedalIcon = createIcon("icon-gaspedal", iconSize)
  val thirdPedalLabel = new Label("3")
  val thirdPedalValue = new Label("0")
  val thirdPedalUnit = new Label("%")
  val thirdPedal = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(thirdPedalLabel, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(new Label("", thirdPedalIcon, Alignment.Center), BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(thirdPedalValue, BorderPanel.Position.Center)
    }

    contents += new BorderPanel {
      add(thirdPedalUnit, BorderPanel.Position.Center)
    }
  }

  val allPedals = new FlowPanel {
    contents += relPedal
    contents += firstPedal
    contents += secondPedal
    contents += thirdPedal
  }
  //endRegion PedalsInfo
  //endregion AllComponentsCreation

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new GridPanel(1, 2) {
      contents += new GridPanel(1, 2) {
        contents += batteryAndLoadInfo
        contents += tempInfo
      }
      contents += allMilInfo
    }

    contents += speedAndRuntimeInfo

    contents += new GridPanel(1, 2) {
      contents += new BorderPanel {
        add(actionButtons, BorderPanel.Position.South)
      }

      contents += new BorderPanel {
        add(allPedals, BorderPanel.Position.East)
      }
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid == "0101") {
      val MILStatus = OBDModel.pid0101(res)
      milValue.selected = MILStatus
      milValue.text = if (MILStatus) "On" else "Off"
    } else if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "0105" => engineTempValue.text = formattedResult
        case "010D" => speedValue.text = formattedResult
        case "011F" => {
          val total = calculatedResult._1
          val minutes = scala.math.floor(total / 60.0)
          val seconds = total - (minutes * 60)

          engineRuntimeValue.text = minutes + "m " + seconds + "s"
        }
        case "0121" => milDistanceValue.text = formattedResult
        case "0142" => batteryVoltValue.text = formattedResult
        case "0143" => loadValue.text = formattedResult
        case "0146" => ambientTempValue.text = formattedResult
        case "0149" => firstPedalValue.text = formattedResult
        case "014A" => secondPedalValue.text = formattedResult
        case "014B" => thirdPedalValue.text = formattedResult
        case "014D" => milTimeValue.text = formattedResult
        case "015A" => relPedalValue.text = formattedResult
        case "015B" => batteryValue.text = formattedResult
        case _      =>
      }
    }
  }
}

class DTC(implicit serialPort: Serial) extends ControlPanelContent("Diagnostic Trouble Code (DTC)", "icon-dtc") {
  val commands: Seq[String] = Seq("01 4E", "01 30", "01 31")

  //region DistanceDTC
  val distanceLabel = new Label("Distance since DTC cleared:")
  val distanceValue = new Label("0")
  val distanceUnit = new Label("km")
  val distanceInfo = new FlowPanel {
    contents += distanceLabel
    contents += distanceValue
    contents += distanceUnit
  }
  //endRegion DistanceDTC

  //region TimeDTC
  val timeLabel = new Label("Time since DTC cleared:")
  val timeValue = new Label("0")
  val timeUnit = new Label("min")
  val timeInfo = new FlowPanel {
    contents += timeLabel
    contents += timeValue
    contents += timeUnit
  }
  //endRegion TimeDTC

  //region Warmup
  val warmUpLabel = new Label("# of warm up since DTC cleared:")
  val warmUpValue = new Label("0")
  val warmUpUnit = new Label("x")
  val warmUpInfo = new FlowPanel {
    contents += warmUpLabel
    contents += warmUpValue
    contents += warmUpUnit
  }
  //endRegion Warmup

  val distTimeWarmup = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(distanceInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(timeInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(warmUpInfo, BorderPanel.Position.East)
    }
  }

  val getDTC = new Button("Get DTC")
  val DTCs = new Table(Array(Array("", "")).asInstanceOf[Array[Array[Any]]], Seq("DTC", "Description"))
  val clearDTC = new Button("Clear DTC / MIL")
  val DTCInfo = new BoxPanel(Orientation.Vertical) {
    border = EmptyBorder(10)
    contents += new BorderPanel {
      add(getDTC, BorderPanel.Position.West)
    }

    contents += VStrut(10)
    contents += new ScrollPane(DTCs)
    contents += VStrut(10)

    contents += new BorderPanel {
      add(clearDTC, BorderPanel.Position.East)
    }
  }

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }
    contents += distTimeWarmup
    contents += DTCInfo
  }

  listenTo(getDTC, clearDTC)
  reactions += {
    case ButtonClicked(`getDTC`) => {
      serialPort.messenger ! "03"
    }

    case ButtonClicked(`clearDTC`) => {
      serialPort.messenger ! "04"
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid == "03") {
      val dtcResult = OBDModel.getDTC(res)
      val dtcDesc = OBDDB.db withSession {
        for {
          dd <- DTCDetails if ((dd.dtcId is dtcResult(0).toString) && (dd.id is dtcResult.substring(1)))
          val desc = dd.description.asColumnOf[String]
        } yield desc
      }

      DTCs.model.setValueAt(dtcResult, 0, 0)
      DTCs.model.setValueAt(dtcDesc.first, 0, 1)
    } else if (pid == "04") {
      // nothing to do. This mode will only return OK.
    } else if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "0130" => warmUpValue.text = formattedResult
        case "0131" => distanceValue.text = formattedResult
        case "014E" => timeValue.text = formattedResult
        case _      =>
      }
    }
  }
}

class Chart(implicit serialPort: Serial) extends ControlPanelContent("Charts", "icon-chart") {
  val commands: Seq[String] = Seq()

  val datas: XYSeries = new XYSeries("Grafik pergerakan nilai x vs y")

  val dataset: XYSeriesCollection = new XYSeriesCollection()
  dataset.addSeries(datas)

  def getSelection = {
    for (pid <- ObdscanScala.powerModePID)
      yield pid.substring(0, 5)
  }

  val ySelection = getSelection
  val xSelection = "Time" :: ySelection
  val xAxisSelection = new AutoCompleteComboBox(xSelection.toArray)
  val yAxisSelection = new AutoCompleteComboBox(ySelection.toArray)
  val viewChartBtn = new Button("View Chart")
  val stopChartBtn = new Button("Stop Chart")
  val xAxisLabel = new Label("X Axis: ")
  val xAxisValue = new Label("")
  val yAxisLabel = new Label("Y Axis: ")
  val yAxisValue = new Label("")

  val disclaimer = new Label("Terdapat jeda antara instruksi pada sumbu x dan sumbu y, yang ditunjukkan oleh anotasi titik.")

  val chart = new LinearChart("XY Chart", "X Axis", "Y Axis", dataset, PlotOrientation.VERTICAL, true, true, false)

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += xAxisSelection
      contents += yAxisSelection
      contents += viewChartBtn
      contents += stopChartBtn
    }

    contents += VStrut(25)

    contents += new BorderPanel {
      add(disclaimer, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new BoxPanel(Orientation.Vertical) {
      contents += new FlowPanel {
        contents += xAxisLabel
        contents += xAxisValue
      }
      contents += new FlowPanel {
        contents += yAxisLabel
        contents += yAxisValue
      }
    }

    contents += VStrut(25)

    contents += chart
  }

  override val communicator = actor {
    loop {
      react {
        case 'start => {
          if (xAxisSelection.selection.item != "Time")
            serialPort.messenger ! xAxisSelection.selection.item
          serialPort.messenger ! yAxisSelection.selection.item
        }
        case 'stop => exit
      }
    }
  }

  def viewChartOperation = {
    super.startOperation
  }

  def stopChartOperation = {
    super.stopOperation
  }

  // serve as temp variable so that
  // we can restore the removed choise when
  // we need to add it back later (when the
  // x or y axis change selection).
  var currentRemovedX = ""
  var currentRemovedY = ""

  listenTo(xAxisSelection.selection, yAxisSelection.selection)
  listenTo(viewChartBtn, stopChartBtn)
  reactions += {
    case SelectionChanged(`xAxisSelection`) => {
      if (xAxisSelection.selection.item != "Time") {
        if (currentRemovedY != "") yAxisSelection.peer.addItem(currentRemovedY)

        val axisText = xAxisSelection.selection.item
        val axisDescription = getPIDDescription(axisText)
        xAxisValue.text = axisDescription
        yAxisSelection.peer.removeItem(axisText)
        currentRemovedY = axisText
      } else {
        xAxisValue.text = "Time"
      }
    }
    case SelectionChanged(`yAxisSelection`) => {
      if (currentRemovedX != "") xAxisSelection.peer.addItem(currentRemovedX)

      val axisText = yAxisSelection.selection.item
      val axisDescription = getPIDDescription(axisText)
      yAxisValue.text = axisDescription
      xAxisSelection.peer.removeItem(axisText)
      currentRemovedX = axisText
    }
    case ButtonClicked(`viewChartBtn`) => {
      chart.changeChartTitle("OBD Data Chart: " + yAxisValue.text + " vs " + xAxisValue.text)
      chart.changeXAxisLabel(xAxisValue.text)
      chart.changeYAxisLabel(yAxisValue.text)

      datas.clear()

      viewChartOperation
    }
    case ButtonClicked(`stopChartBtn`) => {
      stopChartOperation
    }
  }

  def changeSchedule(interval: Int) = {
    sched = scheduler(interval) {
      communicator ! 'start
    }
  }

  def getPIDDescription(pid: String) = {
    val command = pid.substring(3)
    val mode = pid.substring(0, 2)
    var result = ""

    OBDDB.db withSession {
      for {
        pi <- Query(PID) if ((pi.id.asColumnOf[String] is command) && (pi.modeId.asColumnOf[String] is mode))
        val desc = pi.description.asColumnOf[String]
      } {
        result = desc
      }
    }

    result
  }

  var count = 0
  var x = 0.0
  var y = 0.0
  var time = 0.0
  var elapsedTime: Long = 0
  override def responseAction(txt: String) = {
    import org.jfree.chart.annotations.XYTextAnnotation

    val (pid, res) = serialPort.parseQueryResult(txt)
    val calculatedResult = OBDModel.pidFunctions(pid)(res)

    if (xAxisSelection.selection.item == "Time") {
      datas.add(time, calculatedResult._1)
      elapsedTime = serialPort.elapsedQueryTime + Serial.SerialPortDelay

      val xyTextAnnotation = new XYTextAnnotation(elapsedTime.toString + "ms", time, calculatedResult._1)
      chart.getPlot.addAnnotation(xyTextAnnotation)

      time = time + 2.0
    } else {
      if (count < 2) {
        if (count == 0) {
          x = calculatedResult._1
        }
        if (count == 1) {
          y = calculatedResult._1
        }
        count = count + 1
      }
      if (count == 2) {
        elapsedTime = serialPort.elapsedQueryTime + Serial.SerialPortDelay
        datas.add(x, y)

        val xyTextAnnotation = new XYTextAnnotation(elapsedTime.toString + "ms", x, y)
        chart.getPlot.addAnnotation(xyTextAnnotation)

        count = 0
        elapsedTime = 0
      }
    }

    chart.repaint
  }

  override def startOperation = {
    serialPort.removeListener
    serialPort.addListener(this)
  }

  override def stopOperation = {
    serialPort.removeListener
    stopChartOperation
  }
}

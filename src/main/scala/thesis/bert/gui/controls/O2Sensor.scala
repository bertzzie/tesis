package thesis.bert.gui.controls

import java.text.DecimalFormat
import javax.swing.BoxLayout
import scala.swing._
import Swing._
import thesis.bert.gui.components._
import thesis.bert.gui.ControlPanel
import thesis.bert.gui.ControlPanelContent
import thesis.bert.lib.serial.Serial
import thesis.bert.model.OBDModel

class O2SensorExistence(implicit serialPort: Serial) extends ControlPanelContent("O2 Sensor Existance", "icon-like") {
  val commands: Seq[String] = Seq("01 13", "01 1D")

  val bank1 = new Label("Bank 1")
  val bank2 = new Label("Bank 2")
  val bank3 = new Label("Bank 3")
  val bank4 = new Label("Bank 4")

  bank1.font = new Font("Arial", 0, 18)
  bank2.font = new Font("Arial", 0, 18)
  bank3.font = new Font("Arial", 0, 18)
  bank4.font = new Font("Arial", 0, 18)

  val sensor1Bank1Label = new Label("Sensor 1")
  val sensor1Bank1Value = new IndicatorLamp

  val sensor2Bank1Label = new Label("Sensor 2")
  val sensor2Bank1Value = new IndicatorLamp

  val sensor3Bank1Label = new Label("Sensor 3")
  val sensor3Bank1Value = new IndicatorLamp

  val sensor4Bank1Label = new Label("Sensor 4")
  val sensor4Bank1Value = new IndicatorLamp

  val sensor1Bank2Label = new Label("Sensor 1")
  val sensor1Bank2Value = new IndicatorLamp

  val sensor2Bank2Label = new Label("Sensor 2")
  val sensor2Bank2Value = new IndicatorLamp

  val sensor3Bank2Label = new Label("Sensor 3")
  val sensor3Bank2Value = new IndicatorLamp

  val sensor4Bank2Label = new Label("Sensor 4")
  val sensor4Bank2Value = new IndicatorLamp

  val sensor1Bank3Label = new Label("Sensor 1")
  val sensor1Bank3Value = new IndicatorLamp

  val sensor2Bank3Label = new Label("Sensor 2")
  val sensor2Bank3Value = new IndicatorLamp

  val sensor3Bank3Label = new Label("Sensor 3")
  val sensor3Bank3Value = new IndicatorLamp

  val sensor4Bank3Label = new Label("Sensor 4")
  val sensor4Bank3Value = new IndicatorLamp

  val sensor1Bank4Label = new Label("Sensor 1")
  val sensor1Bank4Value = new IndicatorLamp

  val sensor2Bank4Label = new Label("Sensor 2")
  val sensor2Bank4Value = new IndicatorLamp

  val sensor3Bank4Label = new Label("Sensor 3")
  val sensor3Bank4Value = new IndicatorLamp

  val sensor4Bank4Label = new Label("Sensor 4")
  val sensor4Bank4Value = new IndicatorLamp

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new GridPanel(2, 2) {
      vGap = 25
      hGap = 25

      contents += new BoxPanel(Orientation.Vertical) {
        contents += new BorderPanel {
          add(bank1, BorderPanel.Position.Center)
        }

        contents += VStrut(10)

        contents += new GridPanel(2, 4) {
          vGap = 10
          hGap = 10

          contents += sensor1Bank1Label
          contents += sensor2Bank1Label
          contents += sensor3Bank1Label
          contents += sensor4Bank1Label

          contents += sensor1Bank1Value
          contents += sensor2Bank1Value
          contents += sensor3Bank1Value
          contents += sensor4Bank1Value
        }
      }

      contents += new BoxPanel(Orientation.Vertical) {
        contents += new BorderPanel {
          add(bank2, BorderPanel.Position.Center)
        }

        contents += VStrut(10)

        contents += new GridPanel(2, 4) {
          vGap = 10
          hGap = 10

          contents += sensor1Bank2Label
          contents += sensor2Bank2Label
          contents += sensor3Bank2Label
          contents += sensor4Bank2Label

          contents += sensor1Bank2Value
          contents += sensor2Bank2Value
          contents += sensor3Bank2Value
          contents += sensor4Bank2Value
        }
      }

      contents += new BoxPanel(Orientation.Vertical) {
        contents += new BorderPanel {
          add(bank3, BorderPanel.Position.Center)
        }

        contents += VStrut(10)

        contents += new GridPanel(2, 4) {
          vGap = 10
          hGap = 10

          contents += sensor1Bank3Label
          contents += sensor2Bank3Label
          contents += sensor3Bank3Label
          contents += sensor4Bank3Label

          contents += sensor1Bank3Value
          contents += sensor2Bank3Value
          contents += sensor3Bank3Value
          contents += sensor4Bank3Value
        }
      }

      contents += new BoxPanel(Orientation.Vertical) {
        contents += new BorderPanel {
          add(bank4, BorderPanel.Position.Center)
        }

        contents += VStrut(10)

        contents += new GridPanel(2, 4) {
          vGap = 10
          hGap = 10

          contents += sensor1Bank4Label
          contents += sensor2Bank4Label
          contents += sensor3Bank4Label
          contents += sensor4Bank4Label

          contents += sensor1Bank4Value
          contents += sensor2Bank4Value
          contents += sensor3Bank4Value
          contents += sensor4Bank4Value
        }
      }
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid != "00" && res != 0) {
      val result = OBDModel.pid0113(res)

      pid match {
        case "0113" => {
          sensor1Bank1Value.changeIcon(result(0))
          sensor2Bank1Value.changeIcon(result(1))
          sensor3Bank1Value.changeIcon(result(2))
          sensor4Bank1Value.changeIcon(result(3))
          sensor1Bank2Value.changeIcon(result(4))
          sensor2Bank2Value.changeIcon(result(5))
          sensor3Bank2Value.changeIcon(result(6))
          sensor4Bank2Value.changeIcon(result(7))
        }
        case "011D" => {
          sensor1Bank3Value.changeIcon(result(0))
          sensor2Bank3Value.changeIcon(result(1))
          sensor3Bank3Value.changeIcon(result(2))
          sensor4Bank3Value.changeIcon(result(3))
          sensor1Bank4Value.changeIcon(result(4))
          sensor2Bank4Value.changeIcon(result(5))
          sensor3Bank4Value.changeIcon(result(6))
          sensor4Bank4Value.changeIcon(result(7))
        }
        case _ =>
      }
    }
  }
}

class O2SensorVoltage(implicit serialPort: Serial) extends ControlPanelContent("O2 Sensor Voltage", "icon-voltage") {
  val commands: Seq[String] = Seq(
    "01 14",
    "01 15",
    "01 16",
    "01 17",
    "01 18",
    "01 19",
    "01 1A",
    "01 1B",
    "01 46",
    "01 55",
    "01 56",
    "01 57",
    "01 58"
  )

  //region BankSensorInfo
  //region Bank1Info
  val bank1 = new Label("Bank 1")
  bank1.font = new Font("Arial", 0, 18)
  val sensor1Bank1Label = new Label("Sensor 1")
  val sensor1Bank1Value = new Label("1.2v / 90%")

  val sensor2Bank1Label = new Label("Sensor 2")
  val sensor2Bank1Value = new Label("1.2v / 90%")

  val sensor3Bank1Label = new Label("Sensor 3")
  val sensor3Bank1Value = new Label("1.2v / 90%")

  val sensor4Bank1Label = new Label("Sensor 4")
  val sensor4Bank1Value = new Label("1.2v / 90%")

  val bank1Info = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(bank1, BorderPanel.Position.Center)
    }

    contents += new GridPanel(2, 4) {
      hGap = 15
      vGap = 5

      contents += sensor1Bank1Label
      contents += sensor2Bank1Label
      contents += sensor3Bank1Label
      contents += sensor4Bank1Label

      contents += sensor1Bank1Value
      contents += sensor2Bank1Value
      contents += sensor3Bank1Value
      contents += sensor4Bank1Value
    }
  }
  //endRegion Bank1Info

  //region Bank2Info
  val bank2 = new Label("Bank 2")
  bank2.font = new Font("Arial", 0, 18)
  val sensor1Bank2Label = new Label("Sensor 1")
  val sensor1Bank2Value = new Label("1.2v / 90%")

  val sensor2Bank2Label = new Label("Sensor 2")
  val sensor2Bank2Value = new Label("1.2v / 90%")

  val sensor3Bank2Label = new Label("Sensor 3")
  val sensor3Bank2Value = new Label("1.2v / 90%")

  val sensor4Bank2Label = new Label("Sensor 4")
  val sensor4Bank2Value = new Label("1.2v / 90%")

  val bank2Info = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(bank2, BorderPanel.Position.Center)
    }

    contents += new GridPanel(2, 4) {
      hGap = 15
      vGap = 5

      contents += sensor1Bank2Label
      contents += sensor2Bank2Label
      contents += sensor3Bank2Label
      contents += sensor4Bank2Label

      contents += sensor1Bank2Value
      contents += sensor2Bank2Value
      contents += sensor3Bank2Value
      contents += sensor4Bank2Value
    }
  }
  //endRegion Bank2Info

  val banksInfo = new GridPanel(1, 2) {
    hGap = 15
    vGap = 15

    contents += bank1Info
    contents += bank2Info
  }
  //endRegion BankInfo

  //region SecondarySensorInfo
  //region ShortTermSecondarySensorInfo
  val shortTermSecondarySensor = new Label("Short Term Secondary Sensor")
  shortTermSecondarySensor.font = new Font("Arial", 0, 18)
  val stssBank1Label = new Label("Bank 1")
  val stssBank1Value = new Label("90 %")

  val stssBank2Label = new Label("Bank 2")
  val stssBank2Value = new Label("90 %")

  val stssBank3Label = new Label("Bank 3")
  val stssBank3Value = new Label("90 %")

  val stssBank4Label = new Label("Bank 4")
  val stssBank4Value = new Label("90 %")

  val shortTermSecondarySensorInfo = new BoxPanel(Orientation.Vertical) {
    border = EmptyBorder(15)

    contents += new BorderPanel {
      add(shortTermSecondarySensor, BorderPanel.Position.Center)
    }

    contents += new GridPanel(2, 4) {
      contents += stssBank1Label
      contents += stssBank2Label
      contents += stssBank3Label
      contents += stssBank4Label

      contents += stssBank1Value
      contents += stssBank2Value
      contents += stssBank3Value
      contents += stssBank4Value
    }
  }
  //endRegion ShortTermSecondarySensorInfo

  //region LongTermSecondarySensor
  val longTermSecondarySensor = new Label("Long Term Secondary Sensor")
  longTermSecondarySensor.font = new Font("Arial", 0, 18)
  val ltssBank1Label = new Label("Bank 1")
  val ltssBank1Value = new Label("90 %")

  val ltssBank2Label = new Label("Bank 2")
  val ltssBank2Value = new Label("90 %")

  val ltssBank3Label = new Label("Bank 3")
  val ltssBank3Value = new Label("90 %")

  val ltssBank4Label = new Label("Bank 4")
  val ltssBank4Value = new Label("90 %")

  val longTermSecondarySensorInfo = new BoxPanel(Orientation.Vertical) {
    border = EmptyBorder(15)

    contents += new BorderPanel {
      add(longTermSecondarySensor, BorderPanel.Position.Center)
    }

    contents += new GridPanel(2, 4) {
      contents += ltssBank1Label
      contents += ltssBank2Label
      contents += ltssBank3Label
      contents += ltssBank4Label

      contents += ltssBank1Value
      contents += ltssBank2Value
      contents += ltssBank3Value
      contents += ltssBank4Value
    }
  }
  //endRegion LongTermSecondarySensor

  val secondarySensorInfo = new BoxPanel(Orientation.Vertical) {
    contents += shortTermSecondarySensorInfo

    contents += VStrut(15)

    contents += longTermSecondarySensorInfo
  }
  //endRegion SecondarySensorInfo

  //region AmbientTemp
  val ambientTempLabel = new Label("Ambient Temp")
  ambientTempLabel.font = new Font("Arial", 0, 24)
  val ambientTempValue = new Label("00 c")
  ambientTempValue.font = new Font("Arial", 0, 32)

  val ambientTempInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(ambientTempLabel, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(ambientTempValue, BorderPanel.Position.Center)
    }
  }
  //endRegion AmbientTemp

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += banksInfo

    contents += VStrut(25)

    contents += new GridBagPanel {
      import GridBagPanel._

      val c = new Constraints
      c.weightx = 0.5

      c.fill = Fill.Horizontal
      c.gridx = 0
      c.gridy = 0
      layout(secondarySensorInfo) = c

      c.fill = Fill.Horizontal
      c.gridx = 1
      c.gridy = 0
      layout(ambientTempInfo) = c
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val decimalFormatter = new DecimalFormat("#.##")
      val roundFormatter = new DecimalFormat("#")

      def sensorBank(result: Tuple4[Double, Double, Double, Double], lbl: Label) = {
        val (volt, perc) = (result._1, result._2)
        lbl.text = decimalFormatter.format(volt) + "v / " + decimalFormatter.format(perc) + "%"
      }

      def secondarySensor(result: Tuple4[Double, Double, Double, Double], firstLbl: Label, secondLbl: Label) = {
        val (v1, v2) = (result._1, result._2)
        firstLbl.text = roundFormatter.format(v1) + "%"
        secondLbl.text = roundFormatter.format(v2) + "%"
      }

      pid match {
        case "0114" => sensorBank(calculatedResult, sensor1Bank1Value)
        case "0115" => sensorBank(calculatedResult, sensor2Bank1Value)
        case "0116" => sensorBank(calculatedResult, sensor3Bank1Value)
        case "0117" => sensorBank(calculatedResult, sensor4Bank1Value)
        case "0118" => sensorBank(calculatedResult, sensor1Bank2Value)
        case "0119" => sensorBank(calculatedResult, sensor2Bank2Value)
        case "011A" => sensorBank(calculatedResult, sensor3Bank2Value)
        case "011B" => sensorBank(calculatedResult, sensor4Bank2Value)
        case "0146" => ambientTempValue.text = roundFormatter.format(calculatedResult._1) + " C"
        case "0155" => secondarySensor(calculatedResult, stssBank1Value, stssBank3Value)
        case "0156" => secondarySensor(calculatedResult, ltssBank1Value, ltssBank3Value)
        case "0157" => secondarySensor(calculatedResult, stssBank2Value, stssBank4Value)
        case "0158" => secondarySensor(calculatedResult, ltssBank2Value, ltssBank4Value)
        case _      =>
      }
    }
  }
}

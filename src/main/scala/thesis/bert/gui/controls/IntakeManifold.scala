package thesis.bert.gui.controls

import java.text.DecimalFormat
import scala.swing._
import Swing._
import thesis.bert.gui.ControlPanel
import thesis.bert.gui.ControlPanelContent
import thesis.bert.lib.serial.Serial
import thesis.bert.model.OBDModel

class IntakeManifoldInfo(implicit serialPort: Serial) extends ControlPanelContent("Intake Manifold Information", "icon") {
  val commands: Seq[String] = Seq(
    "01 0B",
    "01 0E",
    "01 0F",
    "01 10",
    "01 11",
    "01 45",
    "01 47",
    "01 48",
    "01 4C",
    "01 50",
    "01 84"
  )

  //region PressureTemp
  //region AbsPressureInfo
  val absPressureLabel = new Label("Absolute Pressure:")
  val absPressureValue = new Label("000")
  val absPressureUnit = new Label("kPa")
  val absPressureInfo = new FlowPanel {
    contents += absPressureLabel
    contents += absPressureValue
    contents += absPressureUnit
  }
  //endRegion AbsPressureInfo

  //region SurfaceTempInfo
  val surfaceTempLabel = new Label("Surface Temp:")
  val surfaceTempValue = new Label("00")
  val surfaceTempUnit = new Label("c")
  val surfaceTempInfo = new FlowPanel {
    contents += surfaceTempLabel
    contents += surfaceTempValue
    contents += surfaceTempUnit
  }
  //endRegion SurfaceTempInfo

  //region AirTempInfo
  val airTempLabel = new Label("Air Temp:")
  val airTempValue = new Label("00")
  val airTempUnit = new Label("c")
  val airTempInfo = new FlowPanel {
    contents += airTempLabel
    contents += airTempValue
    contents += airTempUnit
  }
  //endRegion AirTempInfo

  val absPressureAndTemp = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(absPressureInfo, BorderPanel.Position.West)
    }
    contents += new BorderPanel {
      add(surfaceTempInfo, BorderPanel.Position.West)
    }
    contents += new BorderPanel {
      add(airTempInfo, BorderPanel.Position.West)
    }
  }
  //endRegion PressureTemp

  //region Airflow
  //region MaxAirflowRate
  val maxAirflowRateLabel = new Label("Max. Airflow Rate:")
  val maxAirflowRateValue = new Label("0000")
  val maxAirflowRateUnit = new Label("g/s")
  val maxAirflowInfo = new FlowPanel {
    contents += maxAirflowRateLabel
    contents += maxAirflowRateValue
    contents += maxAirflowRateUnit
  }
  //endRegion MaxAirflowRate

  //region AirflowRate
  val airflowRateLabel = new Label("Airflow Rate:")
  val airflowRateValue = new Label("000")
  val airflowRateUnit = new Label("g/s")
  val airflowRateInfo = new FlowPanel {
    contents += airflowRateLabel
    contents += airflowRateValue
    contents += airflowRateUnit
  }
  //endRegion AirflowRate

  val airFlowInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(maxAirflowInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(airflowRateInfo, BorderPanel.Position.East)
    }
  }
  //endRegion Airflow

  //region TimingAndThrottleActuator
  //region Timing
  val timingLabel = new Label("Timing Advance:")
  val timingValue = new Label("00")
  val timingInfo = new FlowPanel {
    contents += timingLabel
    contents += timingValue
  }
  //endRegion Timing

  //region ThrottleActuator
  val throttleLabel = new Label("Throttle Actuator:")
  val throttleValue = new Label("00")
  val throttleUnit = new Label("%")
  val throttleInfo = new FlowPanel {
    contents += throttleLabel
    contents += throttleValue
    contents += throttleUnit
  }
  //endRegion ThrottleActuator

  val timingAndThrottleActuator = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(timingInfo, BorderPanel.Position.West)
    }
    contents += new BorderPanel {
      add(throttleInfo, BorderPanel.Position.West)
    }
  }
  //endRegion TimingAndThrottleActuator

  //region OtherThrottleInfo
  //region ThrottlePos
  val throttlePosLabel = new Label("Throttle Position:")
  val throttlePosValue = new Label("00")
  val throttlePosUnit = new Label("%")
  val throttlePosInfo = new FlowPanel {
    contents += throttlePosLabel
    contents += throttlePosValue
    contents += throttlePosUnit
  }
  //endRegion ThrottlePos

  //region RelThrottlePos
  val relThrottlePosLabel = new Label("Relative Throttle Position:")
  val relThrottlePosValue = new Label("00")
  val relThrottlePosUnit = new Label("%")
  val relThrottlePosInfo = new FlowPanel {
    contents += relThrottlePosLabel
    contents += relThrottlePosValue
    contents += relThrottlePosUnit
  }
  //endRegion RelThrottlePos

  //region AbsThrottlePosA
  val absThrottlePosALabel = new Label("Abs. Throttle Position A:")
  val absThrottlePosAValue = new Label("00")
  val absThrottlePosAUnit = new Label("%")
  val absThrottlePosAInfo = new FlowPanel {
    contents += absThrottlePosALabel
    contents += absThrottlePosAValue
    contents += absThrottlePosAUnit
  }
  //endRegion AbsThrottlePosA

  //region AbsThrottlePosB
  val absThrottlePosBLabel = new Label("Abs. Throttle Position B:")
  val absThrottlePosBValue = new Label("00")
  val absThrottlePosBUnit = new Label("%")
  val absThrottlePosBInfo = new FlowPanel {
    contents += absThrottlePosBLabel
    contents += absThrottlePosBValue
    contents += absThrottlePosBUnit
  }
  //endRegion AbsThrottlePosB

  val otherThrottleInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(throttlePosInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(relThrottlePosInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(absThrottlePosAInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(absThrottlePosBInfo, BorderPanel.Position.East)
    }
  }
  //endRegion OtherThrottleInfo

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new GridPanel(1, 2) {
      contents += absPressureAndTemp
      contents += airFlowInfo
    }

    contents += VStrut(55)

    contents += new GridPanel(1, 2) {
      contents += timingAndThrottleActuator
      contents += otherThrottleInfo
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "010B" => absPressureValue.text = formattedResult
        case "010E" => timingValue.text = formattedResult
        case "010F" => airTempValue.text = formattedResult
        case "0110" => airflowRateValue.text = formattedResult
        case "0111" => throttlePosValue.text = formattedResult
        case "0145" => relThrottlePosValue.text = formattedResult
        case "0147" => absThrottlePosAValue.text = formattedResult
        case "0148" => absThrottlePosBValue.text = formattedResult
        case "014C" => throttleValue.text = formattedResult
        case "0150" => maxAirflowRateValue.text = formattedResult
        case "0184" => absPressureValue.text = formattedResult
        case _      =>
      }
    }
  }
}

package thesis.bert.gui.controls

import java.text.DecimalFormat
import scala.swing._
import Swing._
import thesis.bert.gui.ControlPanel
import thesis.bert.gui.ControlPanelContent
import thesis.bert.lib.serial.Serial
import thesis.bert.model.OBDModel

class FuelInfo(implicit serialPort: Serial) extends ControlPanelContent("Fuel System Information", "icon-info") {
  val commands: Seq[String] = Seq(
    "01 03",
    "01 06",
    "01 07",
    "01 08",
    "01 09",
    "01 2F",
    //      "01 51",
    "01 52"
  )

  //region FuelTypeInfo
  val fuelTypeLabel = new Label("Fuel Type:")
  val fuelTypeValue = new Label("Gasoline")
  fuelTypeValue.font = new Font("Arial", 0, 24)

  val ethanolLabel = new Label("Ethanol")
  val ethanolValue = new Label("00")
  ethanolValue.font = new Font("Arial", 0, 24)
  val ethanolUnit = new Label("%")
  val fuelTypeInfo = new FlowPanel {
    contents += fuelTypeLabel
    contents += fuelTypeValue

    contents += ethanolValue
    contents += ethanolUnit
    contents += ethanolLabel
  }
  //endRegion FuelTypeInfo

  //region FuelLevel
  val fuelLevelLabel = new Label("Fuel Level Input")
  val fuelLevelValue = new Label("00")
  fuelLevelValue.font = new Font("Arial", 0, 24)
  val fuelLevelUnit = new Label("%")
  val fuelLevelInfo = new FlowPanel {
    contents += fuelLevelLabel
    contents += fuelLevelValue
    contents += fuelLevelUnit
  }
  //endRegion FuelLevel

  //region FuelStatus
  val fuelStatusLabel = new Label("Fuel Status:")
  val fuelStatusValue = new Label("Unknown")
  fuelStatusValue.font = new Font("Arial", 0, 32)
  //endRegion FuelStatus

  //region FuelTrim
  val bank1Label = new Label("Bank 1")
  val bank2Label = new Label("Bank 2")

  val shortTermTrim1Label = new Label("Short Term Fuel Trim")
  val shortTermTrim1Value = new Label("00")
  shortTermTrim1Value.font = new Font("Arial", 0, 32)
  val shortTermTrim1Unit = new Label("%")

  val longTermTrim1Label = new Label("Long Term Fuel Trim")
  val longTermTrim1Value = new Label("00")
  longTermTrim1Value.font = new Font("Arial", 0, 32)
  val longTermTrim1Unit = new Label("%")

  val shortTermTrim2Label = new Label("Short Term Fuel Trim")
  val shortTermTrim2Value = new Label("00")
  shortTermTrim2Value.font = new Font("Arial", 0, 32)
  val shortTermTrim2Unit = new Label("%")

  val longTermTrim2Label = new Label("Long Term Fuel Trim")
  val longTermTrim2Value = new Label("00")
  longTermTrim2Value.font = new Font("Arial", 0, 32)
  val longTermTrim2Unit = new Label("%")
  //endRegion FuelTrim

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new GridPanel(1, 2) {
      contents += new BorderPanel {
        add(fuelTypeInfo, BorderPanel.Position.West)
      }
      contents += new BorderPanel {
        add(fuelLevelInfo, BorderPanel.Position.East)
      }
    }

    contents += new BoxPanel(Orientation.Vertical) {
      contents += new BorderPanel {
        add(fuelStatusLabel, BorderPanel.Position.West)
      }
      contents += new BorderPanel {
        add(fuelStatusValue, BorderPanel.Position.Center)
      }
    }

    contents += VStrut(25)

    contents += new GridPanel(1, 2) {
      contents += bank1Label
      contents += bank2Label
    }

    contents += new GridPanel(2, 1) {
      contents += new GridPanel(2, 4) {
        contents += shortTermTrim1Label
        contents += longTermTrim1Label
        contents += shortTermTrim2Label
        contents += longTermTrim2Label

        contents += new FlowPanel {
          contents += shortTermTrim1Value
          contents += shortTermTrim1Unit
        }

        contents += new FlowPanel {
          contents += longTermTrim1Value
          contents += longTermTrim1Unit
        }

        contents += new FlowPanel {
          contents += shortTermTrim2Value
          contents += shortTermTrim2Unit
        }

        contents += new FlowPanel {
          contents += longTermTrim2Value
          contents += longTermTrim2Unit
        }
      }
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid == "0103") {
      val (fuel1Status, fuel2Status) = OBDModel.pid0103(res)
      fuelStatusValue.text = fuel1Status
    } else if (pid == "0151") {
      val fuelType = OBDModel.pid0151(res)
      fuelTypeValue.text = fuelType
    } else if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "0106" => shortTermTrim1Value.text = formattedResult
        case "0107" => longTermTrim1Value.text = formattedResult
        case "0108" => shortTermTrim2Value.text = formattedResult
        case "0109" => longTermTrim2Value.text = formattedResult
        case "012F" => fuelLevelValue.text = formattedResult
        case "0152" => ethanolValue.text = formattedResult
        case _      =>
      }
    }
  }
}

class FuelPressure(implicit serialPort: Serial) extends ControlPanelContent("Fuel Pressure", "icon-pressure") {
  val commands: Seq[String] = Seq(
    "01 0A",
    "01 22",
    "01 59",
    "01 5D"
  )

  //region FuelPressure
  val pressureLabel = new Label("Fuel Pressure:")
  val pressureValue = new Label("000")
  val pressureUnit = new Label("kPa")
  //endRegion FuelPressure

  //region FuelRailPressure
  val railPressureLabel = new Label("Fuel Rail Pressure")
  val railPressureValue = new Label("000")
  val railPressureUnit = new Label("kPa")
  //endRegion FuelRailPressure

  //region FuelRailRelativePressure
  val railRelativePressureLabel = new Label("Fuel Rail Pressure (Relative to Intake Manifold")
  val railRelativePressureValue = new Label("000")
  val railRelativePressureUnit = new Label("kPa")
  //endRegion FuelRailRelativePressure

  //region InjectionTiming
  val injectionTimingLabel = new Label("Fuel Injection Timing")
  val injectionTimingValue = new Label("000")
  //endRegion InjectionTiming

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new FlowPanel {
      contents += pressureLabel
      contents += pressureValue
      contents += pressureUnit
    }

    contents += new FlowPanel {
      contents += railPressureLabel
      contents += railPressureValue
      contents += railPressureUnit
    }

    contents += new FlowPanel {
      contents += railRelativePressureLabel
      contents += railRelativePressureValue
      contents += railRelativePressureUnit
    }

    contents += new FlowPanel {
      contents += injectionTimingLabel
      contents += injectionTimingValue
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "010A" => pressureValue.text = formattedResult
        case "0122" => railRelativePressureValue.text = formattedResult
        case "0159" => railPressureValue.text = formattedResult
        case "015D" => injectionTimingValue.text = formattedResult
        case _      =>
      }
    }
  }
}

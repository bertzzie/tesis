package thesis.bert.gui.controls

import java.text.DecimalFormat
import scala.swing._
import Swing._
import thesis.bert.gui.ControlPanel
import thesis.bert.gui.ControlPanelContent
import thesis.bert.lib.serial.Serial
import thesis.bert.model.OBDModel

class ExhaustInfo(implicit serialPort: Serial) extends ControlPanelContent("Exhaust System Information", "icon") {
  val commands: Seq[String] = Seq(
    "01 12",
    "01 2C",
    "01 2D",
    "01 2E",
    "01 32",
    "01 33",
    "01 3C",
    "01 3D",
    "01 3E",
    "01 3F",
    "01 53"
  )

  //region SecondaryStatus
  val secondaryStatusLabel = new Label("Secondary Air Status:")
  val secondaryStatusValue = new Label("Outside Atmosphere or off")
  secondaryStatusValue.font = new Font("Arial", 0, 32)

  val secondaryStatusInfo = new BoxPanel(Orientation.Vertical) {
    contents += secondaryStatusLabel
    contents += secondaryStatusValue
  }
  //endRegion SecondaryStatus

  //region EGR
  val commandedEGRLabel = new Label("Commanded EGR:")
  val commandedEGRValue = new Label("00")
  val commandedEGRUnit = new Label("%")
  val commandedEGRInfo = new FlowPanel {
    contents += commandedEGRLabel
    contents += commandedEGRValue
    contents += commandedEGRUnit
  }

  val EGRErrorLabel = new Label("EGR Error:")
  val EGRErrorValue = new Label("00")
  val EGRErrorUnit = new Label("%")
  val EGRErrorInfo = new FlowPanel {
    contents += EGRErrorLabel
    contents += EGRErrorValue
    contents += EGRErrorUnit
  }

  val commandedEvapLabel = new Label("Commanded Evap. Purge:")
  val commandedEvapValue = new Label("00")
  val commandedEvapUnit = new Label("%")
  val commandedEvapInfo = new FlowPanel {
    contents += commandedEvapLabel
    contents += commandedEvapValue
    contents += commandedEvapUnit
  }

  val EGRInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(commandedEGRInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(EGRErrorInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(commandedEvapInfo, BorderPanel.Position.East)
    }
  }
  //endRegionEGR

  //region CatalystInfo
  val catalystTemp1 = new Label("Catalyst Temperature")
  val catalystTemp2 = new Label("Catalyst Temperature")
  val bank1 = new Label("Bank 1")
  val bank2 = new Label("Bank 2")
  val sensor1bank1 = new Label("Sensor 1")
  val sensor2bank1 = new Label("Sensor 2")
  val sensor1bank2 = new Label("Sensor 1")
  val sensor2bank2 = new Label("Sensor 2")

  val catalystBank1Sensor1Value = new Label("00")
  catalystBank1Sensor1Value.font = new Font("Arial", 0, 32)
  val catalystBank1Sensor2Value = new Label("00")
  catalystBank1Sensor2Value.font = new Font("Arial", 0, 32)
  val catalystBank2Sensor1Value = new Label("00")
  catalystBank2Sensor1Value.font = new Font("Arial", 0, 32)
  val catalystBank2Sensor2Value = new Label("00")
  catalystBank2Sensor2Value.font = new Font("Arial", 0, 32)

  val catalystInfo = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(catalystTemp1, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(bank1, BorderPanel.Position.Center)
    }
    contents += new GridPanel(2, 2) {
      border = EmptyBorder(5)
      contents += sensor1bank1
      contents += sensor2bank1
      contents += catalystBank1Sensor1Value
      contents += catalystBank1Sensor2Value
    }

    contents += VStrut(20)

    contents += new BorderPanel {
      add(catalystTemp2, BorderPanel.Position.Center)
    }
    contents += new BorderPanel {
      add(bank2, BorderPanel.Position.Center)
    }
    contents += new GridPanel(2, 2) {
      border = EmptyBorder(5)
      contents += sensor1bank2
      contents += sensor2bank2
      contents += catalystBank2Sensor1Value
      contents += catalystBank2Sensor2Value
    }
  }
  //endRegion CatalystInfo

  val barometricPressLabel = new Label("Barometric Pressure:")
  val barometricPressValue = new Label("00")
  val barometricPressUnit = new Label("kPa")
  val barometricPressInfo = new FlowPanel {
    contents += barometricPressLabel
    contents += barometricPressValue
    contents += barometricPressUnit
  }

  val evapVaporPressLabel = new Label("Evap. System Vapor Pressure:")
  val evapVaporPressValue = new Label("00")
  val evapVaporPressUnit = new Label("kPa")
  val evapVaporPressInfo = new FlowPanel {
    contents += evapVaporPressLabel
    contents += evapVaporPressValue
    contents += evapVaporPressUnit
  }

  val absEvapVaporPressLabel = new Label("Absolute Evap. System Vapor Pressure:")
  val absEvapVaporPressValue = new Label("00")
  val absEvapVaporPressUnit = new Label("kPa")
  val absEvapVaporPressInfo = new FlowPanel {
    contents += absEvapVaporPressLabel
    contents += absEvapVaporPressValue
    contents += absEvapVaporPressUnit
  }

  val exhaustPressure = new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(barometricPressInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(evapVaporPressInfo, BorderPanel.Position.East)
    }
    contents += new BorderPanel {
      add(absEvapVaporPressInfo, BorderPanel.Position.East)
    }
  }

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(pageTitle, BorderPanel.Position.Center)
    }

    contents += VStrut(25)

    contents += new GridPanel(1, 2) {
      contents += new BorderPanel {
        add(secondaryStatusInfo, BorderPanel.Position.West)
      }
      contents += new BorderPanel {
        add(EGRInfo, BorderPanel.Position.East)
      }
    }

    contents += VStrut(55)

    contents += new GridBagPanel {
      import GridBagPanel._

      val c = new Constraints
      c.weightx = 0.5

      c.fill = Fill.Horizontal
      c.gridx = 0
      c.gridy = 0
      layout(catalystInfo) = c

      c.fill = Fill.Horizontal
      c.gridx = 1
      c.gridy = 0
      layout(exhaustPressure) = c
    }
  }

  override def responseAction(txt: String) = {
    val (pid, res) = serialPort.parseQueryResult(txt)

    if (pid == "0112") {
      val secondaryStatus = OBDModel.pid0112(res)
      secondaryStatusValue.text = secondaryStatus
    } else if (pid != "00" && res != 0) {
      val calculatedResult = OBDModel.pidFunctions(pid)(res)
      val formatter = new DecimalFormat("#.##")
      val formattedResult = formatter.format(calculatedResult._1)

      pid match {
        case "012C" => commandedEGRValue.text = formattedResult
        case "012D" => EGRErrorValue.text = formattedResult
        case "012E" => commandedEvapValue.text = formattedResult
        case "0132" => evapVaporPressValue.text = formattedResult
        case "0133" => barometricPressValue.text = formattedResult
        case "013C" => catalystBank1Sensor1Value.text = formattedResult
        case "013D" => catalystBank2Sensor1Value.text = formattedResult
        case "013E" => catalystBank1Sensor2Value.text = formattedResult
        case "013F" => catalystBank2Sensor2Value.text = formattedResult
        case "0153" => absEvapVaporPressValue.text = formattedResult
        case _      =>
      }
    }
  }
}

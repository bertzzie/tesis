package thesis.bert.gui.controls

import scala.swing._
import Swing._
import thesis.bert.gui._
import thesis.bert.lib.serial.Serial
import thesis.bert.model._
import org.scalaquery.session._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql._
import org.scalaquery.ql.extended.H2Driver.Implicit._
import org.scalaquery.ql.TypeMapper._
import thesis.bert.lib.helper.BinaryHelper
import javax.swing.table.DefaultTableModel

class Support(implicit serialPort: Serial) extends ControlPanelContent("Engine Standard Support", "icon") {
  val commands: Seq[String] = Seq(
    "01 1C",
    "01 5F",
    "01 00",
    "01 20",
    "01 40",
    "01 60",
    "01 80"
  )

  val titleLabel = new Label(title)

  val supportedStandardLabel = new Label("Supported Standard: ")
  val supportedStandardValue = new Label("")

  val emissionRequirementLabel = new Label("Emission Requirement: ")
  val emissionRequirementValue = new Label("")

  val PIDData = OBDDB.db withSession {
    val pids = for (p <- PID)
      yield p.modeId ~ p.id ~ p.description.asColumnOf[String]

    val datas: List[(String, String, String)] = pids.list
    val arr = for (data <- datas) yield Array(data._1 + " " + data._2, data._3, "No")
    arr.toArray
  }
  val TableHeader = Array("PID", "Description", "Supported").asInstanceOf[Array[Any]]
  val PIDSupport = new Table(PIDData.length, 3) {
    autoResizeMode = Table.AutoResizeMode.AllColumns

    model = new DefaultTableModel(PIDData.length, 3) {
      override def getColumnName(column: Int) = TableHeader(column).toString
    }
  }

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BorderPanel {
      add(supportedStandardLabel, BorderPanel.Position.West)
      add(supportedStandardValue, BorderPanel.Position.West)
      add(emissionRequirementLabel, BorderPanel.Position.East)
      add(emissionRequirementValue, BorderPanel.Position.East)
    }

    initTable

    contents += VStrut(15)
    contents += new ScrollPane(PIDSupport)
  }

  def initTable = {
    for (i <- 0 until PIDData.length) {
      for (j <- 0 until PIDData(i).length) {
        PIDSupport.update(i, j, PIDData(i)(j))
      }
    }
  }

  override def responseAction(txt: String) = {
    val response = txt.substring(txt.indexOf("\n"), txt.lastIndexOf("\n")).trim
    val pidResponse = response.substring(6).replace(" ", "")
    val pid = response.substring(3, 5)
    val result = OBDModel.pidSupport(pid, pidResponse)

    val rc = PIDSupport.rowCount
    for (r <- result) {
      val supported = if (r._2) "Yes" else "No"
      val currentRes = "01 " + BinaryHelper.completeString(r._1, 2)

      for (i <- 0 until rc) {
        if (PIDSupport.model.getValueAt(i, 0) == currentRes) {
          PIDSupport.update(i, 2, supported)
        }
      }
    }

    PIDSupport.update(0, 2, "Yes") // always supported
  }

  override def startOperation = {
    serialPort.removeListener
    serialPort.addListener(this)

    serialPort.messenger ! commands(2)
    serialPort.messenger ! commands(3)
    serialPort.messenger ! commands(4)
    serialPort.messenger ! commands(5)
    serialPort.messenger ! commands(6)

  }

  override def stopOperation = {
    serialPort.removeListener
  }
}
package thesis.bert.gui.components

import scala.swing._
import org.jfree.chart.ChartPanel
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.xy.XYDataset
import org.jfree.chart.ChartFactory
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot

class LinearChart(title: String,
                  xAxisLabel: String,
                  yAxisLabel: String,
                  dataset: XYDataset,
                  orientation: PlotOrientation,
                  legend: Boolean,
                  tooltips: Boolean,
                  urls: Boolean) extends Panel {
  private lazy val chart: JFreeChart = ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend, tooltips, urls)
  override lazy val peer: ChartPanel = new ChartPanel(chart) with SuperMixin

  def this(title: String, xAxisLabel: String, yAxisLabel: String, dataset: XYDataset, orientation: PlotOrientation) = {
    this(title, xAxisLabel, yAxisLabel, dataset, orientation, true, true, false)
  }

  def getPlot = {
    chart.getXYPlot()
  }

  def changeChartTitle(title: String) = {
    chart.setTitle(title)
    repaint
  }

  def changeXAxisLabel(label: String) = {
    val plot: XYPlot = chart.getPlot.asInstanceOf[XYPlot]
    val xAxis = plot.getDomainAxis
    xAxis.setLabel(label)
    repaint
  }

  def changeYAxisLabel(label: String) = {
    val plot: XYPlot = chart.getPlot.asInstanceOf[XYPlot]
    val yAxis = plot.getRangeAxis
    yAxis.setLabel(label)
    repaint
  }
}
package thesis.bert.gui.components

import java.awt.Font
import scala.swing.TextArea
import thesis.bert.lib.serial._
import thesis.bert.lib.helper.BinaryHelper
import thesis.bert.model._
import org.scalaquery.session._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql._
import org.scalaquery.ql.extended.H2Driver.Implicit._
import org.scalaquery.ql.TypeMapper._

class SerialTerminal(implicit serialPort: Serial) extends TextArea with SerialListener {
  rows = 30
  columns = 80
  editable = false
  font = new Font(Font.MONOSPACED, Font.PLAIN, 12)

  override def responseAction(txt: String): Unit = {
    val ecuResponse = serialPort.parseQueryResult(txt)
    val pid = ecuResponse._1
    val res = ecuResponse._2

    pid match {
      case r: String if (r == "0100" || r == "0120" || r == "0140") => OBDModel.pidSupport(r, res.toHexString)
      case r: String if (r == "0101") => {
        val mil = OBDModel.pid0101(res)
        val calculatedResult = if (mil) "hidup." else "mati."

        writeFullResponse(r, calculatedResult, (a: String, b: String, c: String) => "Status " + a + " pada ECU adalah " + b)
      }
      case r: String if (r == "0103") => {
        val (airStatus1, airStatus2) = OBDModel.pid0103(res)
        val func = (a: String, b: String, c: String) => {
          """
          |Hasil pembacaan secondary air status pada sensor 1 adalah: {sensor1}
          |Hasil pembacaan secondary air status pada sensor 2 adalah: {sensor2}
          """.replace("{sensor1}", airStatus1).replace("{sensor2}", airStatus2) stripMargin ('|')
        }

        writeFullResponse(r, "", func)
      }
      case r: String if (r == "0112") => {
        val calculatedResult = OBDModel.pid0112(res)
        val func = (a: String, b: String, c: String) => "Hasil pembacaan dari " + a + " pada ECU adalah " + b + " " + c

        writeFullResponse(r, calculatedResult, func)
      }
      case r: String if (r == "0113") => {
        write("Maaf, perintah ini tidak didukung pada Power Mode. Silahkan gunakan menu O2Sensor pada Standard mode.")
      }
      case r: String if (r == "011C") => {
        val calculatedResult = OBDModel.pid011c(res)
        val func = (a: String, b: String, c: String) => "Hasil pembacaan dari " + a + " pada ECU adalah " + b + " " + c

        writeFullResponse(r, calculatedResult, func)
      }
      case r: String if (r == "0151") => {
        val calculatedResult = OBDModel.pid0151(res)
        val func = (a: String, b: String, c: String) => "Hasil pembacaan dari " + a + " pada ECU adalah " + b + " " + c

        writeFullResponse(r, calculatedResult, func)
      }
      case r: String => {
        val calculatedResult = OBDModel.pidFunctions(r)(res)
        val func = (a: String, b: String, c: String) => "Nilai dari " + a + " pada ECU adalah " + b + " " + c

        writeFullResponse(r, calculatedResult._1.toString, func)
      }
    }
  }

  def writeFullResponse(result: String, calculatedValue: String, f: (String, String, String) => String) = {
    OBDDB.db withSession {
      val modeId = result.substring(0, 2).trim
      val pid = result.substring(2, 4).trim

      for (
        p <- PID if ((p.id is pid) && (p.modeId is modeId));
        val desc = p.description.asColumnOf[String];
        val unit = p.unit.asColumnOf[String]
      ) {
        val command = modeId + " " + pid + " (" + desc + ")"
        val response = f.apply(desc, calculatedValue, unit)

        write("Perintah yang anda kirimkan: " + command)
        write("------------------------------------------------------")
        write(response)
        write("")
        write("Menunggu perintah berikutnya...")
        write("")
        write("")
        write("")
      }
    }
  }

  override def readSerial: String = {
    serialPort.queryResult
  }

  def write(txt: String) = {
    this.append(txt + "\n")
    this.caret.position = this.text.length // autoscroll
  }
}
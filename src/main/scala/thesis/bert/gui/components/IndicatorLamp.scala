package thesis.bert.gui.components

import scala.swing._
import javax.swing.ImageIcon

class IndicatorLamp() extends Label {
  var on = false

  text = ""
  icon = changeIcon(on)

  def changeIcon(status: Boolean) = {
    if (on) IndicatorLamp.OnIcon else IndicatorLamp.OffIcon
  }
}

object IndicatorLamp {
  val ImagePath = "/indicator/"
  val OnImage = getClass.getResource(ImagePath + "on.png")
  val OffImage = getClass.getResource(ImagePath + "off.png")

  val OnIcon = new ImageIcon(IndicatorLamp.OnImage)
  val OffIcon = new ImageIcon(IndicatorLamp.OffImage)
}
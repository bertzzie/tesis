package thesis.bert.gui.components

import javax.swing.JComboBox
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator
import scala.swing.ComboBox
import scala.swing.Component
import scala.swing.event.SelectionChanged
import scala.swing.Publisher
import scala.swing.Swing

class AutoCompleteComboBox(items: Array[String]) extends Component {
  override lazy val peer = new JComboBox(items) with SuperMixin
  AutoCompleteDecorator.decorate(peer)

  object selection extends Publisher {
    def index: Int = peer.getSelectedIndex
    def index_=(n: Int) = peer.setSelectedIndex(n)
    def item: String = peer.getSelectedItem.toString
    def item_=(i: String) = peer.setSelectedItem(i)

    peer.addActionListener(Swing.ActionListener { e =>
      // the "comboBoxEdited" event is hard coded in
      // JComboBox. That's why it's hardcoded here too :(
      if (e.getActionCommand == "comboBoxEdited")
        publish(SelectionChanged(AutoCompleteComboBox.this))
    })
  }
}
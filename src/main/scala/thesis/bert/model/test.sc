package thesis.bert.model

object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  OBDModel.pid010C(65535)                         //> res0: (Double, Double, Double, Double) = (16383.75,0.0,0.0,0.0)
  OBDModel.pid0110(0)                             //> res1: (Double, Double, Double, Double) = (0.0,0.0,0.0,0.0)
  OBDModel.pid0122(65535)                         //> res2: (Double, Double, Double, Double) = (5177.265,0.0,0.0,0.0)
  OBDModel.pid0132(65535)                         //> res3: (Double, Double, Double, Double) = (-8128.25,0.0,0.0,0.0)
  OBDModel.pid0132(65280)                         //> res4: (Double, Double, Double, Double) = (-8192.0,0.0,0.0,0.0)
  OBDModel.pid0132(32767)                         //> res5: (Double, Double, Double, Double) = (8191.75,0.0,0.0,0.0)
  OBDModel.pidFunctions("0104")                   //> res6: Int => (Double, Double, Double, Double) = <function1>
  OBDModel.pidFunctions("0105")                   //> res7: Int => (Double, Double, Double, Double) = <function1>
  //OBDModel.pidFunctions
  
  val res = OBDModel.pidSupport("40", "BE1FA813") //> res  : scala.collection.immutable.IndexedSeq[(java.lang.String, Boolean)] = 
                                                  //| Vector((41,true), (42,false), (43,true), (44,true), (45,true), (46,true), (4
                                                  //| 7,true), (48,false), (49,false), (4A,false), (4B,false), (4C,true), (4D,true
                                                  //| ), (4E,true), (4F,true), (50,true), (51,true), (52,false), (53,true), (54,fa
                                                  //| lse), (55,true), (56,false), (57,false), (58,false), (59,false), (5A,false),
                                                  //|  (5B,false), (5C,true), (5D,false), (5E,false), (5F,true), (60,true))
  res.length                                      //> res8: Int = 32
  
  import org.scalaquery.session._
  import org.scalaquery.session.Database.threadLocalSession
  import org.scalaquery.ql._
  import org.scalaquery.ql.extended.H2Driver.Implicit._
  
  val db = OBDDB.db                               //> db  : org.scalaquery.session.Database = org.scalaquery.session.Database$$ano
                                                  //| n$2@6d0e6c33
  db withSession {
    Query(PIDMode) foreach {
      case (id, description) => println("  " + id + " -> " + description)
    }
  }
  
  OBDModel.getDTC(32760)
}
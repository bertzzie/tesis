package thesis.bert.model

import thesis.bert.lib.serial._
import thesis.bert.lib.helper.BinaryHelper
import scala.collection.immutable.HashMap

class OBDModel(implicit serialPort: Serial) extends SerialListener {

  /*
   * This method gets response from RS232 and then calculate
   * the return value and then returns it as Integer, according to
   * the OBD Specification.
   * 
   * Because of RxTx we will need to format the return value to normal
   * hex value first. Here's the return value from RxTx:
   * 
   * <pre>
   * 01 0
   *
   * 41 00 FF FF FF FF
   * >
   * </pre>
   * 
   * And we want it to become:
   * 
   * <pre>
   * 41 00 FF FF FF FF
   * </pre>
   * 
   * hence the trim and substring at the beginning of this method.
   * 
   */
  override def responseAction(txt: String): Unit = {
    val response = txt.substring(txt.indexOf("\n"), txt.lastIndexOf("\n")).trim

    val pid = response.substring(0, 5)
    val pidResponse = response.substring(6)
    val v = Integer.parseInt(pidResponse, 16)

    println("Return value (calculated) : " + OBDModel.pidFunctions(pid)(v))
  }

  override def readSerial: String = {
    serialPort.queryResult
  }
}

object OBDModel {

  def getDTC(d: Int) = {
    val (byte1, byte2) = BinaryHelper.twoBytesSplit(d)
    val byte1Bin = BinaryHelper.completeString(byte1.toBinaryString, 8)
    val byte2Bin = BinaryHelper.completeString(byte2.toBinaryString, 8)

    val firstDTCCode = (byte1Bin(7).toString + byte1Bin(6).toString) match {
      case "00" => "P"
      case "01" => "C"
      case "10" => "B"
      case "11" => "U"
    }
    val secondDTCCode = (byte1Bin(5).toString + byte1Bin(4).toString) match {
      case "00" => "0"
      case "01" => "1"
      case "10" => "2"
      case "11" => "3"
    }
    val thirdDTCCode = BinaryHelper.binaryStringtoInt(byte1Bin.substring(0, 4).reverse).toHexString
    val fourthDTCCode = BinaryHelper.binaryStringtoInt(byte2Bin.substring(4).reverse).toHexString
    val fifthDTCCode = BinaryHelper.binaryStringtoInt(byte2Bin.substring(0, 4).reverse).toHexString

    firstDTCCode + secondDTCCode + thirdDTCCode + fourthDTCCode + fifthDTCCode
  }

  /*
   * - both arguments are hex string. This is for consistency in this function.
   *   remmeber, all other places uses binary string (or Int). Using hex string
   *   is the best approach, IMO, because screw consistency. Nothing is consistent
   *   in the OBD Spec anyway.
   * - returns a Vector(Tuple2), with format: (PID (in hex), is the PID Supported)
   * 
   * TODO: Convert this comment to ScalaDoc. Currently no internet, 
   * can't write in ScalaDoc without documentation (yet).
   */
  def pidSupport(pid: String, returnedBit: String) = {
    val binaryReturnedBit = BinaryHelper.hexStringtoBinaryString(returnedBit)

    for (i <- 0 until binaryReturnedBit.length) yield {
      val checkedPid = BinaryHelper.hexStringtoInt(pid) + i + 1
      val isPidSupported = binaryReturnedBit(i) match {
        case '0' => false
        case '1' => true
      }

      (checkedPid.toHexString.toUpperCase, isPidSupported)
    }
  }

  /*
   * We only get MIL Status here, since it's the only one we needed
   */
  def pid0101(d: Int): Boolean = {
    val bits = BinaryHelper.fourBytesSplit(d)
    val byte1 = bits._1.toBinaryString

    val mil = byte1.charAt(7) match {
      case '0' => false
      case '1' => true
    }

    mil
  }

  def pid0103(d: Int): (String, String) = {
    def getStatus(binaryString: String): String = {
      binaryString.indexOf("1") match {
        case 0 => "Open loop due to insufficient engine temperature"
        case 1 => "Closed loop, using oxygen sensor feedback to determine fuel mix"
        case 2 => "Open loop due to engine load OR fuel cut due to deceleration"
        case 3 => "Open loop due to system failure"
        case 4 => "Closed loop, using at least one oxygen sensor but there is a fault in the feedback system"
        case _ => "Return value unsupported by OBD-II"
      }
    }
    val bits = BinaryHelper.twoBytesSplit(d)
    val byte1 = bits._1
    val byte2 = bits._2

    (getStatus(byte1.toBinaryString), getStatus(byte2.toBinaryString))
  }

  def pid0112(d: Int): String = {
    d.toBinaryString.indexOf("1") match {
      case 0 => "Upstream of catalytic converter"
      case 1 => "Downstream of catalytic converter"
      case 2 => "From the outside atmosphere or off"
      case _ => "Return value unsupported by OBD-II"
    }
  }

  /*
   * Also used by:
   * 01 1D
   */
  def pid0113(d: Int) = {
    for (i <- d.toBinaryString)
      yield if (i == '1') true else false
  }

  def pid011c(d: Int) = {
    d.toBinaryString match {
      case "00000001" => "OBD-II as defined by the CARB"
      case "00000010" => "OBD as defined by the EPA"
      case "00000011" => "OBD and OBD-II"
      case "00000100" => "OBD-I"
      case "00000101" => "Not meant to comply with any OBD standard"
      case "00000110" => "EOBD (Europe)"
      case "00000111" => "EOBD and OBD-II"
      case "00001000" => "EOBD and OBD"
      case "00001001" => "EOBD, OBD and OBD II"
      case "00001010" => "JOBD (Japan)"
      case "00001011" => "JOBD and OBD II"
      case "00001100" => "JOBD and EOBD"
      case "00001101" => "JOBD, EOBD, and OBD II"
    }
  }

  def pid0151(d: Int): String = {
    d.toHexString match {
      case "1"  => "Gasoline"
      case "2"  => "Methanol"
      case "3"  => "Ethanol"
      case "4"  => "Diesel"
      case "5"  => "LPG"
      case "6"  => "CNG"
      case "7"  => "Propane"
      case "8"  => "Electric"
      case "9"  => "Bifuel running Gasoline"
      case "A"  => "Bifuel running Methanol"
      case "B"  => "Bifuel running Ethanol"
      case "C"  => "Bifuel running LPG"
      case "D"  => "Bifuel running CNG"
      case "E"  => "Bifuel running Prop"
      case "F"  => "Bifuel running Electricity"
      case "10" => "Bifuel mixed gas / electric"
      case "11" => "Hybrid Gasoline"
      case "12" => "Hybrid Ethanol"
      case "13" => "Hybrid Diesel"
      case "14" => "Hybrid Electric"
      case "15" => "Hybrid Mixed Fuel"
      case "16" => "Hybrid Regenerative"
    }
  }

  /*
   * Also used by:
   * 01 11
   * 01 2C
   * 01 2E
   * 01 2F
   * 01 45
   * 01 47
   * 01 48
   * 01 49
   * 01 4A
   * 01 4B
   * 01 4C
   * 01 52
   * 01 5A
   * 01 5B
   */
  val pid0104 = (d: Int) => (d * 100.0 / 255.0, 0.0, 0.0, 0.0)

  /*
   * Also used by:
   * 01 0F
   * 01 46
   * 01 5C
   */
  val pid0105 = (d: Int) => (d - 40.0, 0.0, 0.0, 0.0)

  /*
   * Also used by:
   * 01 07
   * 01 08
   * 01 09
   * 01 2D
   */
  val pid0106 = (d: Int) => ((d - 128.0) * 100.0 / 128.0, 0.0, 0.0, 0.0)

  /*
   * Also used by:
   * 01 30
   */
  val pid010A = (d: Int) => (d * 3.0, 0.0, 0.0, 0.0)

  /*
   * Also used by:
   * 01 0B
   * 01 0D
   * 01 33
   */
  val pid010B = (d: Int) => (d.toDouble, 0.0, 0.0, 0.0)

  val pid010C = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    (((bits._1 * 256.0) + bits._2) / 4.0, 0.0, 0.0, 0.0)
  }

  val pid010E = (d: Int) => (d / 2.0 - 64.0, 0.0, 0.0, 0.0)

  val pid0110 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    (((bits._1 * 256.0) + bits._2) / 100.0, 0.0, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 15
   * 01 16
   * 01 17
   * 01 18
   * 01 19
   * 01 1A
   * 01 1B
   */
  val pid0114 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    val voltage = bits._1 / 200.0
    val shortTermFuelTrim = bits._2 match {
      case 255 => 0.0
      case _   => (bits._2 - 128.0) * 100.0 / 128.0
    }

    (voltage, shortTermFuelTrim, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 21
   * 01 31
   * 01 4D
   * 01 4E
   */
  val pid011F = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    ((bits._1 * 256.0) + bits._2, 0.0, 0.0, 0.0)
  }

  val pid0122 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    (((bits._1 * 256.0) + bits._2) * 0.079, 0.0, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 59
   */
  val pid0123 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    (((bits._1 * 256.0) + bits._2) * 10, 0.0, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 25
   * 01 26
   * 01 27
   * 01 28
   * 01 29
   * 01 2A
   * 01 2B
   */
  val pid0124 = (d: Int) => {
    val bits = BinaryHelper.fourBytesSplit(d)
    val eqRatio = ((bits._1 * 256.0) + bits._2) * 2.0 / 65535.0
    val voltage = ((bits._3 * 256.0) + bits._4) * 8.0 / 65535.0

    (eqRatio, voltage, 0.0, 0.0)
  }

  val pid0132 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    val a = BinaryHelper.signedBinaryStringtoInt(bits._1.toBinaryString)
    (((a * 256.0) + bits._2) / 4.0, 0.0, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 35
   * 01 36
   * 01 37
   * 01 38
   * 01 39
   * 01 3A
   * 01 3B
   */
  val pid0134 = (d: Int) => {
    val bits = BinaryHelper.fourBytesSplit(d)
    val eqRatio = ((bits._1 * 256) + bits._2) / 32786.0
    val current = ((bits._3 * 256) + bits._4) / 256.0 - 128.0

    (eqRatio, current, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 3D
   * 01 3E
   * 01 3F
   */
  val pid013c = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (((bits._1 * 256) + bits._2) / 10.0 - 40.0, 0.0, 0.0, 0.0)
  }

  val pid0142 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (((bits._1 * 256.0) + bits._2) / 1000.0, 0.0, 0.0, 0.0)
  }

  val pid0143 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (((bits._1 * 256.0) + bits._2) * 100.0 / 256.0, 0.0, 0.0, 0.0)
  }

  val pid0144 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (((bits._1 * 256.0) + bits._2) / 32768.0, 0.0, 0.0, 0.0)
  }

  val pid014f = (d: Int) => {
    val bits = BinaryHelper.fourBytesSplit(d)

    (bits._1.toDouble, bits._2.toDouble, bits._3.toDouble, bits._4 * 10.0)
  }

  val pid0150 = (d: Int) => {
    val bits = BinaryHelper.fourBytesSplit(d)

    (bits._1 * 10.0, 0.0, 0.0, 0.0)
  }

  val pid0154 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (bits._1 * 256.0 + bits._2 - 32768.0, 0.0, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 56
   * 01 57
   * 01 58
   */
  val pid0155 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)
    val func = (v: Int) => (v - 128.0) * 100.0 / 128.0

    (func(bits._1), func(bits._2), 0.0, 0.0)
  }

  val pid015d = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    ((((bits._1 * 256.0) + bits._2) - 26880.0) / 128.0, 0.0, 0.0, 0.0)
  }

  val pid015e = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (((bits._1 * 256.0) + bits._2) * 0.05, 0.0, 0.0, 0.0)
  }

  /*
   * Also used by:
   * 01 62
   */
  val pid0161 = (d: Int) => (d - 125.0, 0.0, 0.0, 0.0)

  val pid0163 = (d: Int) => {
    val bits = BinaryHelper.twoBytesSplit(d)

    (bits._1 * 256.0 + bits._2, 0.0, 0.0, 0.0)
  }

  val pidFunctions: HashMap[String, Int => Tuple4[Double, Double, Double, Double]] = HashMap(
    "0104" -> pid0104,
    "0105" -> pid0105,
    "0106" -> pid0106,
    "0107" -> pid0106,
    "0108" -> pid0106,
    "0109" -> pid0106,
    "010A" -> pid010A,
    "010B" -> pid010B,
    "010C" -> pid010C,
    "010D" -> pid010B,
    "010E" -> pid010E,
    "010F" -> pid0105,
    "0110" -> pid0110,
    "0111" -> pid0104,
    "0114" -> pid0114,
    "0115" -> pid0114,
    "0116" -> pid0114,
    "0117" -> pid0114,
    "0118" -> pid0114,
    "0119" -> pid0114,
    "011A" -> pid0114,
    "011B" -> pid0114,
    "011F" -> pid011F,
    "0121" -> pid011F,
    "0122" -> pid0122,
    "0123" -> pid0123,
    "0124" -> pid0124,
    "0125" -> pid0124,
    "0126" -> pid0124,
    "0127" -> pid0124,
    "0128" -> pid0124,
    "0129" -> pid0124,
    "012A" -> pid0124,
    "012B" -> pid0124,
    "012C" -> pid0104,
    "012D" -> pid0106,
    "012E" -> pid0104,
    "012F" -> pid0104,
    "0130" -> pid010A,
    "0131" -> pid011F,
    "0132" -> pid0132,
    "0133" -> pid010B,
    "0134" -> pid0134,
    "0135" -> pid0134,
    "0136" -> pid0134,
    "0137" -> pid0134,
    "0138" -> pid0134,
    "0139" -> pid0134,
    "013A" -> pid0134,
    "013B" -> pid0134,
    "013C" -> pid013c,
    "013D" -> pid013c,
    "013E" -> pid013c,
    "013F" -> pid013c,
    "0142" -> pid0142,
    "0143" -> pid0143,
    "0144" -> pid0144,
    "0145" -> pid0104,
    "0146" -> pid0105,
    "0147" -> pid0104,
    "0148" -> pid0104,
    "0149" -> pid0104,
    "014A" -> pid0104,
    "014B" -> pid0104,
    "014C" -> pid0104,
    "014D" -> pid011F,
    "014E" -> pid011F,
    "014F" -> pid014f,
    "0150" -> pid0150,
    "0152" -> pid0104,
    "0154" -> pid0154,
    "0155" -> pid0155,
    "0156" -> pid0155,
    "0157" -> pid0155,
    "0158" -> pid0155,
    "0159" -> pid0123,
    "015A" -> pid0104,
    "015B" -> pid0104,
    "015C" -> pid0105,
    "015D" -> pid015d,
    "015E" -> pid015e,
    "0161" -> pid0161,
    "0162" -> pid0161,
    "0163" -> pid0163
  )
}
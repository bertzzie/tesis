package thesis.bert.model

import java.sql.Clob
import org.scalaquery.session._
import org.scalaquery.ql.extended.H2Driver
import org.scalaquery.ql.extended.H2Driver._
import org.scalaquery.ql.extended.H2Driver.Implicit._
import org.scalaquery.ql.extended.{ ExtendedTable => Table }
import org.scalaquery.ql.TypeMapper
import org.scalaquery.ql._

object OBDDB {
  // apparantely, H2DB embedded mode means the database file still must be called
  // from an absolute path. Can't make it to run relative from our jar.
  // This is the only compromise we make. Remember to make an installer so user do
  // no work.
  // TODO: INSTALLER
  val db = Database.forURL("jdbc:h2:~/obdiiscan;USER=sa;PASSWORD=", driver = "org.h2.Driver")
}

object DTC extends Table[(String, Clob)]("DTC") {
  def id = column[String]("ID", O NotNull, O PrimaryKey, O DBType ("CHAR(1)"))
  def description = column[Clob]("DESCRIPTION")

  def * = id ~ description
}

object DTCDetails extends Table[(String, String, Clob)]("DTCDETAILS") {
  def id = column[String]("ID", O DBType ("CHAR(4)"), O NotNull)
  def dtcId = column[String]("DTCID", O DBType ("CHAR(1)"), O NotNull)
  def description = column[Clob]("DESCRIPTION")

  def * = id ~ dtcId ~ description
  def pk = primaryKey("pk_DTCDetails", id ~ dtcId)
  def fkDTC = foreignKey("fk_DTCDetails_DTC", dtcId, DTC)(_.id)
}

object PIDMode extends Table[(String, Clob)]("PIDMODE") {
  def id = column[String]("ID", O NotNull, O PrimaryKey, O DBType ("CHAR(2)"))
  def description = column[Clob]("DESCRIPTION")

  def * = id ~ description
}

object PID extends Table[(String, String, Clob, Clob, Clob, Clob, Clob)]("PID") {
  def id = column[String]("PID", O NotNull, O DBType ("VARCHAR(4)"))
  def modeId = column[String]("MODEID", O NotNull, O DBType ("CHAR(2)"))
  def byteReturned = column[Clob]("BYTERETURNED")
  def description = column[Clob]("DESCRIPTION")
  def min = column[Clob]("MIN")
  def max = column[Clob]("MAX")
  def unit = column[Clob]("UNIT")

  def * = id ~ modeId ~ byteReturned ~ description ~ min ~ max ~ unit
  def pk = primaryKey("pk_PID", id ~ modeId)
  def fkPID = foreignKey("fk_PID_PIDMode", modeId, PIDMode)(_.id)
}
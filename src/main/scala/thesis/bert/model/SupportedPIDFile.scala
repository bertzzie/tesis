package thesis.bert.model

import java.io.BufferedOutputStream
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.charset.Charset
import java.nio.file.StandardOpenOption._

object SupportedPIDFile {
  val FilePath = Paths.get("supportedPID.obd")
  val CharSet = Charset.forName("US-ASCII")

  def checkFile: Boolean = {
    Files.exists(FilePath)
  }

  def addPID(pid: String) = {
    val data = (pid + "\r\n").getBytes

    val out = new BufferedOutputStream(Files.newOutputStream(FilePath, CREATE, APPEND))
    out.write(data, 0, data.length)
    out.close
  }

  def loadItems: List[String] = {
    val reader = Files.newBufferedReader(FilePath, CharSet)

    var line = reader.readLine()
    var result = List[String]()
    do {
      result = line :: result
      line = reader.readLine()
    } while (line != null)

    result
  }

  def deleteFile = {
    Files.delete(FilePath)
  }
}
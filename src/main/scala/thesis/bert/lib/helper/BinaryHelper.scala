package thesis.bert.lib.helper

object BinaryHelper {
  val ONE_BYTE = 8
  val TWO_BYTES = ONE_BYTE * 2
  val THREE_BYTES = ONE_BYTE * 3
  val FOUR_BYTES = TWO_BYTES * 2

  def twoBytesSplit(b: Int): (Int, Int) = {
    val bitString = completeString(b.toBinaryString, TWO_BYTES)

    (binaryStringtoInt(bitString.substring(0, ONE_BYTE)), binaryStringtoInt(bitString.substring(ONE_BYTE)))
  }

  def fourBytesSplit(b: Int): (Int, Int, Int, Int) = {
    val bitString = completeString(b.toBinaryString, FOUR_BYTES)

    val byte1 = binaryStringtoInt(bitString.substring(0, ONE_BYTE))
    val byte2 = binaryStringtoInt(bitString.substring(ONE_BYTE, TWO_BYTES))
    val byte3 = binaryStringtoInt(bitString.substring(TWO_BYTES, THREE_BYTES))
    val byte4 = binaryStringtoInt(bitString.substring(THREE_BYTES))

    (byte1, byte2, byte3, byte4)
  }

  def completeString(value: String, bitCount: Int): String = {
    val valLength = value.length
    "0" * (bitCount - valLength) + value
  }

  def binaryStringtoInt(binString: String): Int = {
    Integer.parseInt(binString, 2)
  }

  def hexStringtoInt(hexString: String): Int = {
    Integer.parseInt(hexString, 16)
  }

  def hexStringtoBinaryString(hexString: String): String = {
    val result = for (chr <- hexString; val h = chr.toString)
      yield completeString(hexStringtoInt(h).toBinaryString, 4)

    result.mkString
  }

  // the range must be from -128 to 127.
  def signedBinaryStringtoInt(binString: String): Int = {
    // we actually wants byte, but Int is good enough.
    require(binaryStringtoInt(binString) <= 255)

    // make sure that binString is always 8 bits
    val bitString = if (binString.length < ONE_BYTE)
      "0" * (8 - binString.length) + binString
    else
      binString

    val sign = if (bitString.substring(0, 1) == "0") "+" else "-"

    if (sign == "-")
      Integer.parseInt(sign + bitString.substring(1), 2) - 1
    else
      Integer.parseInt(sign + bitString.substring(1), 2)
  }
}
package thesis.bert.lib.helper

object binaryhelpertest {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  BinaryHelper.signedBinaryStringtoInt("11111111")//> res0: Int = -128
  BinaryHelper.signedBinaryStringtoInt("01111111")//> res1: Int = 127
  BinaryHelper.binaryStringtoInt("11111111")      //> res2: Int = 255
  BinaryHelper.binaryStringtoInt("01111111")      //> res3: Int = 127
  BinaryHelper.hexStringtoBinaryString("FFFFFFFF")//> res4: String = 11111111111111111111111111111111
  BinaryHelper.hexStringtoBinaryString("BE1FA813")//> res5: String = 10111110000111111010100000010011
}
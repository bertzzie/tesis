package thesis.bert.lib.serial

import gnu.io._

trait SerialListener extends SerialPortEventListener {
  override def serialEvent(event: SerialPortEvent) = {
    event.getEventType() match {
      case SerialPortEvent.DATA_AVAILABLE => {
        val response = readSerial
        responseWait(response)
      }
    }
  }

  // this resp variable is used for waiting
  // the full result from ECU. Sometimes the
  // ECU returns result one by one.
  var resp = ""
  def responseWait(response: String) = {
    if (!response.contains(">")) {
      resp = resp + response
    } else {
      if (resp == "") resp = response

      responseAction(resp)
      resp = ""
    }
  }

  def responseAction(txt: String): Unit

  def readSerial: String
}
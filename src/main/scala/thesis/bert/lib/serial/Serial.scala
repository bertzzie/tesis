package thesis.bert.lib.serial

import collection.JavaConversions._
import gnu.io._
import java.io._
import java.util.TooManyListenersException
import scala.actors.Actor._

class Serial private (portName: String, baudRate: Int, dataBits: Int, stopBits: Int, parity: Int, flowControl: Int) {
  private val portId = CommPortIdentifier.getPortIdentifier(portName)
  private val serial = portId.open("Serial Connection from OBDScan", 5000).asInstanceOf[SerialPort]
  setPortParameters(baudRate, dataBits, stopBits, parity, flowControl)

  private val istream = serial.getInputStream
  private val ostream = serial.getOutputStream

  private var startQueryTime: Long = 0

  val messenger = actor {
    loop {
      react {
        case m: String => {
          query(m)
          Thread.sleep(Serial.SerialPortDelay)
        }
      }
    }
  }

  def this(portName: String, baudRate: Int = 115200) = this(portName, baudRate,
    SerialPort.DATABITS_8,
    SerialPort.STOPBITS_1,
    SerialPort.PARITY_NONE,
    SerialPort.FLOWCONTROL_NONE)

  def close = {
    try {
      istream.close
      ostream.close
    } catch {
      case ioe: IOException => // don't care, it's ended already
    }

    serial.close
  }

  private def query(command: String) = {
    ostream.write(command.getBytes)
    ostream.write("\r\n".getBytes) // neccessary for Serial port.

    startQueryTime = System.currentTimeMillis
  }

  /*
   * Method to get the time elapsed for one query, or time elapsed
   * since query started.
   * 
   * If you want to use this to get time elapsed for one query, call
   * right after a SerialEvent.
   */
  def elapsedQueryTime: Long = {
    System.currentTimeMillis - startQueryTime
  }

  def queryResult: String = {
    try {
      val availableBytes = istream.available
      val buffer = new Array[Byte](availableBytes)

      if (availableBytes > 0) {
        istream.read(buffer, 0, availableBytes)
      }
      new String(buffer, 0, availableBytes)
    } catch {
      case ioe: IOException => "Something wrong! Please try again."
    }
  }

  def parseQueryResult(qr: String): (String, Int) = {
    val result = qr.replaceAll(" ", "").replaceAll(Serial.LineSeparator, "").replaceAll(">", "").trim
    val cleanResult = result.substring(4)

    val realResult = cleanResult match {
      case r: String if r == "OK"          => r.substring(2) // For command with no PID and returns only OK. (eq: 04)
      case r: String if r.startsWith("03") => r.substring(2) // just for command 03 (with no PID)
      case r: String if r.startsWith("0")  => r.substring(8)
      case r: String if r.startsWith("4")  => r.substring(4)
      case _                               => "00"
    }

    val pid = cleanResult match {
      case r: String if r.startsWith("4")  => "0" + r.substring(1, 4)
      case r: String if r.startsWith("03") => "03"
      case r: String if r.startsWith("04") => "04"
      case r: String                       => "00"
    }

    (pid, Integer.parseInt(realResult, 16))
  }

  def addListener(listener: SerialPortEventListener) = {
    try {
      serial.addEventListener(listener)
      serial.notifyOnDataAvailable(true)
    } catch {
      case tm: TooManyListenersException => println("Too many listener")
    }
  }

  def removeListener = {
    serial.removeEventListener
  }

  private def setPortParameters(br: Int, db: Int, sb: Int, p: Int, fc: Int) = {
    serial.setSerialPortParams(br, db, sb, p)
    serial.setFlowControlMode(fc)
  }
}

object Serial {
  def connect(portName: String, baudRate: Int = 115200): Serial = {
    try {
      val ret = new Serial(portName, baudRate)
      ret
    } catch {
      case nsp: NoSuchPortException               => throw new IOException("Serial port not found")
      case piu: PortInUseException                => throw new IOException("Serial port is in use by othre program.")
      case ioe: IOException                       => throw new IOException("Unknown IO Exception. " + ioe.getMessage)
      case uco: UnsupportedCommOperationException => throw new IOException("Unsupported command. Recheck your serial port driver.")
    }
  }

  def getSerialPorts: Seq[String] = {
    val comm = CommPortIdentifier.getPortIdentifiers
    val ports = comm.toSeq

    for {
      p <- ports
      port = p.asInstanceOf[CommPortIdentifier]
      if port.getPortType == CommPortIdentifier.PORT_SERIAL
    } yield port.getName
  }

  val SerialPortDelay = 1500
  val LineSeparator = "\r\n"
}
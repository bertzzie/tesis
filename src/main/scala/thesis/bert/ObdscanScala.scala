package thesis.bert

import scala.actors.Actor._
import scala.swing._
import java.awt.Dimension
import scala.swing.event.ButtonClicked
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import scala.swing.event.ButtonClicked
import thesis.bert.gui._
import thesis.bert.gui.components.SerialTerminal
import thesis.bert.lib.serial._
import scala.swing.event.ButtonClicked
import thesis.bert.lib.helper.BinaryHelper
import thesis.bert.model._
import org.scalaquery.session._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql._
import org.scalaquery.ql.extended.H2Driver.Implicit._
import org.scalaquery.ql.TypeMapper._
import scala.swing.event.SelectionChanged
import scala.swing.event.SelectionChanged
import scala.swing.event.FontChanged
import scala.swing.ProgressBar
import thesis.bert.model.SupportedPIDFile

object ObdscanScala extends SimpleSwingApplication {
  implicit var serialPort: Serial = null
  var powerModePID: List[String] = List()

  def initPowerMode(implicit serialPort: Serial) = {
    new PowerMode
  }

  def initStandardMode(implicit serialPort: Serial) = {
    new StandardMode
  }

  def userWantsOldPIDData: Boolean = {
    import scala.swing.Dialog._

    val choice = List("Ya", "Tidak")

    val answer = showOptions(null,
      "Program menemukan daftar PID yang telah didukung. Apakah anda ingin menggunakan daftar ini?",
      "PID Didukung Ditemukan",
      entries = choice,
      initial = 1) match {
        case Result.Yes => true
        case Result.No  => false
      }

    answer
  }

  lazy val standardMode: StandardMode = initStandardMode
  lazy val powerMode: PowerMode = initPowerMode

  def top = new MainFrame {
    size = new Dimension(800, 600)
    title = "OBD Scan Tool"

    val appMode = new ToggleButton {
      text = "Power Mode"
      selected = false
    }
    appMode.enabled = false

    contents = new SplitPane {
      orientation = Orientation.Horizontal
      dividerSize = 0

      leftComponent = new GridPanel(1, 2) {
        contents += new FlowPanel {
          contents += new Label("OBD Scan Tool")
          contents += appMode
        }

        contents += new FlowPanel {
          val connect = new Button("Connect")
          val disconnect = new Button("Disconnect")
          val portsSelection = new ComboBox(Serial.getSerialPorts)

          disconnect.enabled = false

          contents += new Label("Choose port: ")
          contents += portsSelection
          contents += connect
          contents += disconnect

          listenTo(connect, disconnect)
          reactions += {
            case ButtonClicked(`connect`) => {
              serialPort = Serial.connect(portsSelection.selection.item, 9600)

              var userWantOld = false
              if (SupportedPIDFile.checkFile) {
                if (userWantsOldPIDData) {
                  serialPort.messenger ! "AT Z"

                  SupportedPIDFile.loadItems foreach { data =>
                    val db = OBDDB.db
                    db withSession {
                      val modeId = data.substring(0, 2)
                      val pid = data.substring(3)
                      for (p <- PID if ((p.id is pid) && (p.modeId is modeId)); val desc = p.description.asColumnOf[String]) {
                        val text = modeId + " " + pid + "(" + desc + ")"
                        powerMode.addItem(text)
                        powerModePID ::= text
                      }
                    }
                  }

                  userWantOld = true
                } else {
                  SupportedPIDFile.deleteFile
                  checkPIDSupport
                }
              } else {
                checkPIDSupport
              }

              portsSelection.enabled = false
              connect.enabled = false
              disconnect.enabled = true

              if (userWantOld) {
                appMode.enabled = true
              } else {
                appMode.enabled = false
              }
            }

            case ButtonClicked(`disconnect`) => {
              serialPort.close
              portsSelection.enabled = true
              connect.enabled = true
              disconnect.enabled = false
              appMode.enabled = false
            }
          }
        }

      } // MainFrame / SplitPane / leftComponent

      rightComponent = new FlowPanel {
        contents += new Label("Not initialized yet. Please select COM Port and connect.")
      }

      listenTo(appMode)
      reactions += {
        case ButtonClicked(`appMode`) => {
          if (appMode.selected) {
            rightComponent = powerMode

            standardMode.deactivate
            powerMode.activate
          } else {
            rightComponent = standardMode

            initStandardModeEvents
            powerMode.deactivate
            standardMode.activate
          }
        }
      }

      def initStandardModeEvents = {
        listenTo(standardMode.selection)
        reactions += {
          case SelectionChanged(`standardMode`) => {
            standardMode.deactivateAll
            standardMode.activate
          }
        }
      }

      def checkPIDSupport(implicit serialPort: Serial) = {
        var PIDCount = 0
        val TotalPID = 181

        val progressBar = new ProgressBar {
          min = 0
          max = TotalPID
          label = "Test progress"
          labelPainted = true
        }

        val supportedPID = new SerialTerminal {
          override def responseAction(txt: String) = {
            val response = txt.replaceAll(" ", "").replaceAll(Serial.LineSeparator, "").replaceAll(">", "").trim

            if (!response.contains("ATZ")) {
              PIDCount = PIDCount + 1
              progressBar.value = PIDCount
              progressBar.label = "Tested " + PIDCount + "/" + TotalPID + " PIDs"
            }

            response match {
              case s: String if s.contains("ATZ")  => write("Initializing device...")
              case s: String if s.startsWith("02") => //
              case s: String if s.startsWith("03") => parse0304PID(s)
              case s: String if s.startsWith("04") => parse0304PID(s)
              case s: String if s.startsWith("05") => // parse05PIDs(s)
              case s: String if s.startsWith("09") => //
              case s: String                       => parseSupportedResult(s)
            }

            if (PIDCount == TotalPID) {
              appMode.enabled = true
              write("All PIDs tested!")
            }
          }

          def getSupportStatus(stat: Boolean): String = {
            if (stat) "supported" else "not supported"
          }

          def parse0304PID(response: String) = {
            val result = response.substring(4)

            if (result != "NODATA" && result != "?" && !result.matches("7F0\\d12")) {
              val modeId = response.substring(0, 2)
              val pid = response.substring(2, 5)
              val db = OBDDB.db

              db withSession {
                for (p <- PID if ((p.id is pid) && (p.modeId is modeId)); val desc = p.description.asColumnOf[String]) {
                  val text = modeId + " " + pid + "(" + desc + ")"
                  SupportedPIDFile.addPID(modeId + " " + pid)
                  powerMode.addItem(text)
                  powerModePID ::= text
                }
              }

              write("PID " + modeId + " " + pid + " is supported.")
            }
          }

          def parse05PIDs(response: String) = {
            //            val result = response.substring(4)
            val processedResponse = response match {
              case s: String if s.startsWith("05100") |
                s.startsWith("05101") |
                s.startsWith("05102") |
                s.startsWith("05103") |
                s.startsWith("05104") |
                s.startsWith("05105") |
                s.startsWith("05106") |
                s.startsWith("05107") |
                s.startsWith("05108") |
                s.startsWith("05109") |
                s.startsWith("05110") |
                s.startsWith("05201") |
                s.startsWith("05202") |
                s.startsWith("05203") |
                s.startsWith("05204") |
                s.startsWith("05205") |
                s.startsWith("05206") |
                s.startsWith("05207") |
                s.startsWith("05208") |
                s.startsWith("05209") |
                s.startsWith("05210") => (s.substring(5), "05", s.substring(2, 5))
              case s: String if s.startsWith("05010A") |
                s.startsWith("05010B") |
                s.startsWith("05010C") |
                s.startsWith("05010D") |
                s.startsWith("05010E") |
                s.startsWith("05010F") |
                s.startsWith("05020A") |
                s.startsWith("05020B") |
                s.startsWith("05020C") |
                s.startsWith("05020D") |
                s.startsWith("05020E") |
                s.startsWith("05020F") => (s.substring(6), "05", s.substring(2, 6))
              case _ => ("NODATA", "", "")
            }

            val result = processedResponse._1
            val modeId = processedResponse._2
            val pid = processedResponse._3

            if (result != "NODATA" && result != "?" && !result.matches("7F0\\d12")) {
              //              val modeId = response.substring(0, 2)
              //              val pid = response.substring(2, 4)
              val db = OBDDB.db

              db withSession {
                for (p <- PID if ((p.id is pid) && (p.modeId is modeId)); val desc = p.description.asColumnOf[String]) {
                  val text = modeId + " " + pid + "(" + desc + ")"
                  SupportedPIDFile.addPID(modeId + " " + pid)
                  powerMode.addItem(text)
                  powerModePID ::= text
                }
              }

              write("PID " + modeId + " " + pid + " is supported.")
            }
          }

          def parseSupportedResult(response: String) = {
            val result = response.substring(4)

            if (result != "NODATA" && result != "?" && !result.matches("7F0\\d12")) {
              val modeId = response.substring(0, 2)
              val pid = response.substring(2, 4)
              val db = OBDDB.db

              db withSession {
                for (p <- PID if ((p.id is pid) && (p.modeId is modeId)); val desc = p.description.asColumnOf[String]) {
                  val text = modeId + " " + pid + "(" + desc + ")"
                  SupportedPIDFile.addPID(modeId + " " + pid)
                  powerMode.addItem(text)
                  powerModePID ::= text
                }
              }

              write("PID " + modeId + " " + pid + " is supported.")
            }
          }
        }

        rightComponent = new BoxPanel(Orientation.Vertical) {
          contents += progressBar
          contents += new ScrollPane(supportedPID)
        }

        serialPort.removeListener
        serialPort.addListener(supportedPID)
        actor {
          serialPort.messenger ! "AT Z"

          OBDDB.db withSession {
            for (p <- PID; val modeId = p.modeId; val pid = p.id) {
              serialPort.messenger ! modeId + " " + pid
            }
          }
        }
      }
    }
  }
}
